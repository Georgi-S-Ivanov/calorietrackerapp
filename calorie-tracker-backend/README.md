## Setup

Make sure you are on node v16.10.0 (you can use nvm to manage multiple node versions)

1. `git clone https://Georgi-S-Ivanov@bitbucket.org/Georgi-S-Ivanov/calorietrackerapp.git`

2. Place the `firebase.config.json` file inside the `calorie-tracker-backend/` folder
Hint: Search for `firebase.config.json` inside your gmail to find emails containing the file.

3. Open `calorie-tracker-backend/` in Terminal

4. Run - `npm install`

5. Install firebase-cli if you don't have it already
`npm install -g firebase-tools`

Then Run `firebase login` 

## Running the app

```bash
# development
$ npm run serve
```

The localhost server url will look something like this:
`http://localhost:5001/calorietracker-fe8f5/europe-west1/api`

## Test

```bash
# unit tests
$ npm run test
```

#### Potential issues

If you see this error while for `npm run serve`
`Cannot find module '../../../firebase.config.json' or its corresponding type declarations.`
It means that you have not placed the `firebase.config.json` file inside the root folder of the project.