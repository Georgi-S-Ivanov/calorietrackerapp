import { IdentityToolkitService } from './../../src/modules/firebase/identity-toolkit.service';
import { NewFirebaseUser } from './../../src/modules/firebase/dto/new-firebase-user';
import * as stub from './../stub-factories';
import { SignedInFirebaseUser } from './../../src/modules/firebase/dto/signed-in-firebase-user';

export const identityServiceMockFactory: () => Partial<IdentityToolkitService> =
  () => ({
    signUpUser: jest.fn(async (email): Promise<NewFirebaseUser> => {
      return Promise.resolve(stub.newFirebaseUser(email));
    }),
    signInUser: jest.fn(async (email): Promise<SignedInFirebaseUser> => {
      return Promise.resolve(stub.signedInUser(email));
    }),
  });
