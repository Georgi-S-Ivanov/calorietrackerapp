import { FirestoreDocument } from './../../src/modules/firebase/interfaces/firestore-document';
import { CalorieTrackerService } from './../../src/modules/firebase/calorie-tracker.service';
import { TrackerSettings } from './../../src/modules/tracker/dto/tracker-settings.dto';

export const calorieTrackerServiceMockFactory: () => Partial<CalorieTrackerService> =
  () => ({
    updateTrackerSettings: jest.fn(
      async (
        userId: string,
        settings: TrackerSettings,
      ): Promise<TrackerSettings & FirestoreDocument> => {
        return {
          id: 'settingsId',
          calorieLimit: settings.calorieLimit,
          email: settings.email,
        };
      },
    ),
  });
