import * as admin from 'firebase-admin';
import { Role } from '../../src/enums/role.enum';
import { FirebaseService } from '../../src/modules/firebase/firebase.service';

export const firebaseServiceMockFactory: () => Partial<FirebaseService> =
  () => ({
    auth: authServiceMockFactory() as admin.auth.Auth,
  });

export const authServiceMockFactory: () => Partial<admin.auth.Auth> = jest.fn(
  () => ({
    setCustomUserClaims: jest.fn(async (id, claims): Promise<void> => {
      return Promise.resolve();
    }),
    verifyIdToken: jest.fn(async (token): Promise<any> => {
      return {
        roles: [Role.Admin],
      };
    }),
  }),
);
