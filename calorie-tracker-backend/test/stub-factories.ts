import { Role } from './../src/enums/role.enum';
import { NewFirebaseUser } from './../src/modules/firebase/dto/new-firebase-user';
import { SignedInFirebaseUser } from './../src/modules/firebase/dto/signed-in-firebase-user';
import { SignedInUser } from './../src/modules/user/dto/signed-in-user';

export function expectedSignedInUser(roles: Role[]): SignedInUser {
  return {
    id: 'mockId',
    email: 'mockUser@gmail.com',
    token: 'mockToken.endoded.data',
    refreshToken: 'mockRefresh.token.data',
    expiresIn: '3600',
    roles: roles,
  };
}

export function signedInUser(
  email: string | null = null,
): SignedInFirebaseUser {
  return {
    idToken: 'mockToken.endoded.data',
    email: email ?? 'mockUser@gmail.com',
    refreshToken: 'mockRefresh.token.data',
    expiresIn: '3600',
    localId: 'mockId',
    displayName: '',
    registered: true,
  };
}

export function newFirebaseUser(email: string | null = null): NewFirebaseUser {
  return {
    idToken: 'mockToken.endoded.data',
    email: email ?? 'mockUser@gmail.com',
    refreshToken: 'mockRefresh.token.data',
    expiresIn: '3600',
    localId: 'mockId',
  };
}
