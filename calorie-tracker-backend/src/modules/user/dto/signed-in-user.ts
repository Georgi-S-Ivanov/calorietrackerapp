import { SignedInFirebaseUser } from '../../firebase/dto/signed-in-firebase-user';
import { Role } from '../../../enums/role.enum';

export class SignedInUser {
  readonly id: string;
  readonly email: string;
  readonly token: string;
  readonly refreshToken: string;
  readonly expiresIn: string;
  readonly roles: Role[];

  private constructor(
    id: string,
    email: string,
    token: string,
    refreshToken: string,
    expiresIn: string,
    roles: Role[],
  ) {
    this.id = id;
    this.email = email;
    this.token = token;
    this.refreshToken = refreshToken;
    this.expiresIn = expiresIn;
    this.roles = roles;
  }

  static signedIn(firebaseUser: SignedInFirebaseUser, roles: Role[]) {
    return new SignedInUser(
      firebaseUser.localId,
      firebaseUser.email,
      firebaseUser.idToken,
      firebaseUser.refreshToken,
      firebaseUser.expiresIn,
      roles,
    );
  }
}
