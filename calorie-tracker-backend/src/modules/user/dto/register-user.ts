import {
  ArrayNotEmpty,
  IsArray,
  IsEmail,
  IsEnum,
  IsString,
  MinLength,
} from 'class-validator';
import { Match } from '../../../decorators/match.decorator';
import { Role } from '../../../enums/role.enum';

export class RegisterUser {
  @IsEmail()
  readonly email!: string;

  @IsString()
  @MinLength(6)
  readonly password!: string;

  @IsString()
  @MinLength(6)
  @Match('password')
  readonly confirmPassword!: string;

  @IsArray()
  @ArrayNotEmpty()
  @IsEnum(Role, {
    each: true,
  })
  readonly roles!: Role[];
}
