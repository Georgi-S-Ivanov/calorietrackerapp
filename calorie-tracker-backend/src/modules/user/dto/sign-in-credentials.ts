import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class SignInCredentials {
  @IsEmail()
  readonly email!: string;

  @IsString()
  @IsNotEmpty()
  readonly password!: string;
}
