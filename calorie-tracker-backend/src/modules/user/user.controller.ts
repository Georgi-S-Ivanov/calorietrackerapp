import { Body, Controller, Post } from '@nestjs/common';
import { FirebaseService } from '../firebase/firebase.service';
import { IdentityToolkitService } from '../firebase/identity-toolkit.service';
import { RegisterUser } from './dto/register-user';
import { SignInCredentials } from './dto/sign-in-credentials';
import { SignedInUser } from './dto/signed-in-user';
import { Public } from '../../decorators/public.decorator';
import { CalorieTrackerService } from '../firebase/calorie-tracker.service';

@Controller('user')
export class UserController {
  constructor(
    private identityService: IdentityToolkitService,
    private firebase: FirebaseService,
    private calorieTracker: CalorieTrackerService,
  ) {}

  @Public()
  @Post('/signup')
  async signUpUser(@Body() user: RegisterUser) {
    const firebaseUser = await this.identityService.signUpUser(
      user.email,
      user.password,
    );

    await this.firebase.auth.setCustomUserClaims(firebaseUser.localId, {
      roles: user.roles,
    });

    await this.calorieTracker.updateTrackerSettings(firebaseUser.localId, {
      calorieLimit: 2100,
      email: user.email,
    });

    return this.signInUser(user.email, user.password);
  }

  @Public()
  @Post('/signin')
  async signInUserEndpoint(@Body() signIn: SignInCredentials) {
    return this.signInUser(signIn.email, signIn.password);
  }

  async signInUser(email: string, password: string): Promise<SignedInUser> {
    const firebaseUser = await this.identityService.signInUser(email, password);

    const decodedToken = await this.firebase.auth.verifyIdToken(
      firebaseUser.idToken,
    );

    return SignedInUser.signedIn(firebaseUser, decodedToken.roles);
  }
}
