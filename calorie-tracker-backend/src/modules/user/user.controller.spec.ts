import { Test, TestingModule } from '@nestjs/testing';
import { CalorieTrackerService } from '../firebase/calorie-tracker.service';
import { FirebaseService } from '../firebase/firebase.service';
import { IdentityToolkitService } from '../firebase/identity-toolkit.service';
import { UserController } from './user.controller';
import * as test from './../../../test/test';
import { RegisterUser } from './dto/register-user';
import { Role } from './../../enums/role.enum';

describe('UserController', () => {
  let sut: UserController;
  let identityService: Partial<IdentityToolkitService>;
  let firebase: Partial<FirebaseService>;
  let calorieTracker: Partial<CalorieTrackerService>;

  beforeEach(async () => {
    firebase = test.firebaseServiceMockFactory();
    identityService = test.identityServiceMockFactory();
    calorieTracker = test.calorieTrackerServiceMockFactory();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: FirebaseService,
          useValue: firebase,
        },
        {
          provide: IdentityToolkitService,
          useValue: identityService,
        },
        {
          provide: CalorieTrackerService,
          useValue: calorieTracker,
        },
      ],
    }).compile();

    sut = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(sut).toBeDefined();
  });

  it('should create a user with admin role', async () => {
    const dto: RegisterUser = {
      email: 'mockUser@gmail.com',
      password: '123456',
      confirmPassword: '123456',
      roles: [Role.Admin],
    };

    const result = await sut.signUpUser(dto);

    expect(result).toEqual(test.stub.expectedSignedInUser([Role.Admin]));
    expect(identityService.signUpUser).toHaveBeenCalledWith<[string, string]>(
      dto.email,
      dto.password,
    );
    expect(firebase.auth?.setCustomUserClaims).toHaveBeenCalledWith<
      [string, { roles: Role[] }]
    >('mockId', { roles: [Role.Admin] });
    expect(calorieTracker.updateTrackerSettings).toHaveBeenCalledWith<
      [string, { calorieLimit: number; email: string }]
    >('mockId', {
      calorieLimit: 2100,
      email: 'mockUser@gmail.com',
    });
    expect(identityService.signInUser).toHaveBeenCalledWith<[string, string]>(
      dto.email,
      dto.password,
    );
    expect(firebase.auth?.verifyIdToken).toHaveBeenCalledWith<[string]>(
      'mockToken.endoded.data',
    );
  });

  it('should sign in existing user', async () => {
    const result = await sut.signInUser('mockUser@gmail.com', '123456');
    expect(result).toEqual(test.stub.expectedSignedInUser([Role.Admin]));
    expect(identityService.signInUser).toHaveBeenCalledWith<[string, string]>(
      'mockUser@gmail.com',
      '123456',
    );
    expect(firebase.auth?.verifyIdToken).toHaveBeenCalledWith<[string]>(
      'mockToken.endoded.data',
    );
  });
});
