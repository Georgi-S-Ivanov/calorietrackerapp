import { Module } from '@nestjs/common';
import { FirebaseModule } from '../firebase/firebase.module';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';

@Module({
  imports: [FirebaseModule],
  providers: [AdminService],
  controllers: [AdminController],
})
export class AdminModule {}
