export class UserAverageCalories {
  id!: string;
  email!: string;
  averageCalories!: number;
}
