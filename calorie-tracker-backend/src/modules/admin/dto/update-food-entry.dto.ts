import { IsNotEmpty, IsString } from 'class-validator';
import { FoodEntry } from './../../tracker/dto/food-entry.dto';

export class UpdateFoodEntry extends FoodEntry {
  @IsString()
  @IsNotEmpty()
  id!: string;
}
