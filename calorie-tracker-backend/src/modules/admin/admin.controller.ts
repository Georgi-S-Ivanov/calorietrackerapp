import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Roles, Role } from 'src/decorators/roles.decorator';
import { CalorieTrackerService } from '../firebase/calorie-tracker.service';
import { FoodEntry } from '../tracker/dto/food-entry.dto';
import { AdminService } from './admin.service';
import { UpdateFoodEntry } from './dto/update-food-entry.dto';

@Controller('admin')
export class AdminController {
  constructor(
    private calorieTracker: CalorieTrackerService,
    private adminService: AdminService,
  ) {}

  @Roles(Role.Admin)
  @Get('entries')
  async getAllEntries() {
    return this.calorieTracker.allEntries();
  }

  @Roles(Role.Admin)
  @Get('entries/weekreport')
  async getWeekReport() {
    return this.adminService.weekComparisonReport();
  }

  @Roles(Role.Admin)
  @Get('entries/average')
  async getAverateCaloriesPerUserForLastWeek() {
    return this.adminService.averageCaloriesPerUserForLastWeek();
  }

  @Roles(Role.Admin)
  @Put('users/:id/entries')
  async updateFoodEntry(
    @Param('id') userId: string,
    @Body() entry: UpdateFoodEntry,
  ) {
    return this.calorieTracker.updateEntry(userId, entry);
  }

  @Roles(Role.Admin)
  @Post('users/:id/entries')
  async createEntryForUser(
    @Param('id') userId: string,
    @Body() entry: FoodEntry,
  ) {
    return this.calorieTracker.createEntry(userId, entry);
  }

  @Roles(Role.Admin)
  @Delete('users/:id/entries/:entryId')
  async deleteEntryForUser(
    @Param('id') userId: string,
    @Param('entryId') entryId: string,
  ) {
    return this.calorieTracker.deleteEntry(userId, entryId);
  }
}
