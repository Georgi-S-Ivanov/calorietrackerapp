import { Injectable } from '@nestjs/common';
import { dateDaysAgo } from './../../utilities/date-calculations';
import { FirebaseService } from '../firebase/firebase.service';
import { firestore } from 'firebase-admin';
import { EntriesByWeek } from './dto/entries-by-week';
import { UserAverageCalories } from './dto/user-average-calories';

@Injectable()
export class AdminService {
  constructor(private firebaseService: FirebaseService) {}

  async weekComparisonReport(): Promise<EntriesByWeek> {
    const foodEntries = await this.firebaseService.db
      .collection('foodEntries')
      .listDocuments();

    const now = new Date();
    const weekAgo = dateDaysAgo(7);
    const twoWeeksAgo = dateDaysAgo(14);

    let weekAgoCount = 0;
    let twoWeeksAgoCount = 0;

    for await (const userDoc of foodEntries) {
      const forUser = userDoc.collection('foodEntriesForUser');
      const entriesWeekAgo = await this.entriesCount(forUser, now, weekAgo);
      const entriesTwoWeeksAgo = await this.entriesCount(
        forUser,
        weekAgo,
        twoWeeksAgo,
      );

      weekAgoCount += entriesWeekAgo;
      twoWeeksAgoCount += entriesTwoWeeksAgo;
    }

    return {
      entriesWeekAgo: weekAgoCount,
      entriesTwoWeeksAgo: twoWeeksAgoCount,
    };
  }

  async averageCaloriesPerUserForLastWeek(): Promise<any> {
    const foodEntries = await this.firebaseService.db
      .collection('foodEntries')
      .listDocuments();

    const now = new Date();
    const weekAgo = dateDaysAgo(7);
    const result = Array<UserAverageCalories>();

    for await (const userDoc of foodEntries) {
      const forUser = userDoc.collection('foodEntriesForUser');
      const userData = (await userDoc.get()).data();
      const entriesWeekAgo = await this.entries(forUser, now, weekAgo);
      const totalCals = entriesWeekAgo.docs.reduce((prev, doc) => {
        return prev + doc.data().calories;
      }, 0);

      result.push({
        id: userData?.userId,
        email: userData?.email,
        averageCalories: this.calculateCalories(totalCals, entriesWeekAgo.size),
      });
    }

    return result;
  }

  private calculateCalories(total: number, entries: number): number {
    if (entries == 0) {
      return 0;
    }

    return total / entries;
  }

  private async entries(
    user: firestore.CollectionReference<firestore.DocumentData>,
    from: Date,
    to: Date,
  ): Promise<firestore.QuerySnapshot<firestore.DocumentData>> {
    return await user.where('date', '<=', from).where('date', '>', to).get();
  }

  private async entriesCount(
    user: firestore.CollectionReference<firestore.DocumentData>,
    from: Date,
    to: Date,
  ): Promise<number> {
    return (await user.where('date', '<=', from).where('date', '>', to).get())
      .size;
  }
}
