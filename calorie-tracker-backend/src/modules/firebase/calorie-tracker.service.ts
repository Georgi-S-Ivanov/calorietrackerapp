import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FilterEntriesQuery } from '../tracker/dto/filter-entries-query';
import { FoodEntry } from '../tracker/dto/food-entry.dto';
import { TrackerSettings } from '../tracker/dto/tracker-settings.dto';
import { FirebaseService } from './firebase.service';
import { FirestoreDocument } from './interfaces/firestore-document';
import { firestore } from 'firebase-admin';
import { UserEntries } from './dto/user-entries';
import { UpdateFoodEntry } from '../admin/dto/update-food-entry.dto';

@Injectable()
export class CalorieTrackerService {
  constructor(private firebaseService: FirebaseService) {}

  async createEntry(
    userId: string,
    entry: FoodEntry,
  ): Promise<FoodEntry & FirestoreDocument> {
    const foodEntries = await this.getFoodEntries(userId);
    const timestamp = firestore.Timestamp.fromDate(new Date(entry.date));
    const added = await foodEntries.add({
      date: timestamp,
      name: entry.name,
      calories: entry.calories,
      photoUrl: entry.photoUrl ?? null,
    });

    return {
      id: added.id,
      date: entry.date,
      name: entry.name,
      calories: entry.calories,
      photoUrl: entry.photoUrl,
    };
  }

  async updateEntry(
    userId: string,
    entry: UpdateFoodEntry,
  ): Promise<FoodEntry & FirestoreDocument> {
    const fEntries = await this.queryForFoodEntries(userId);
    if (fEntries.empty) {
      throw new HttpException(
        `Could not find any food entries for user with id ${userId}`,
        HttpStatus.NOT_FOUND,
      );
    }
    const entriesRef = fEntries.docs[0].ref.collection('foodEntriesForUser');
    const docRef = entriesRef.doc(entry.id);

    if (!(await docRef.get()).exists) {
      throw new HttpException(
        `Food entry with id ${entry.id} was not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    const write = await docRef.update({
      date: firestore.Timestamp.fromDate(new Date(entry.date)),
      name: entry.name,
      calories: entry.calories,
      photoUrl: entry.photoUrl ?? null,
    });

    if (write.writeTime.seconds > 0) {
      return entry;
    } else {
      throw new HttpException(
        `Could not update entry with id ${entry.id}.`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async deleteEntry(
    userId: string,
    entryId: string,
  ): Promise<FoodEntry & FirestoreDocument> {
    const entriesRef = await this.getFoodEntries(userId);
    const docRef = entriesRef.doc(entryId);

    if (!(await docRef.get()).exists) {
      throw new HttpException(
        `Food entry with id ${entryId} was not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    const doc = await docRef.get();
    const data = this.mapFoodEntry(doc);

    const write = await docRef.delete();
    if (write.writeTime.seconds > 0) {
      if (data.photoUrl) {
        this.deletePhoto(data.photoUrl);
      }
      return data;
    } else {
      throw new HttpException(
        `Could not update entry with id ${entryId}.`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async foodEntriesForUser(
    userId: string,
  ): Promise<(FoodEntry & FirestoreDocument)[]> {
    const foodEntries = await (await this.getFoodEntries(userId))
      .orderBy('date', 'desc')
      .get();

    return this.mapFoodEntries(foodEntries);
  }

  async filterFoodEntries(
    userId: string,
    params: FilterEntriesQuery,
  ): Promise<(FoodEntry & FirestoreDocument)[]> {
    const foodEntries = await (await this.getFoodEntries(userId))
      .orderBy('date', 'desc')
      .where('date', '>=', new Date(params.beginDate))
      .where('date', '<', new Date(params.endDate))
      .get();

    return this.mapFoodEntries(foodEntries);
  }

  async updateTrackerSettings(
    userId: string,
    settings: TrackerSettings,
  ): Promise<TrackerSettings & FirestoreDocument> {
    const doc = await this.getOrCreateFoodDocument(userId);

    await doc.ref.update(settings);
    const result = (
      await this.getOrCreateFoodDocument(userId)
    ).data() as TrackerSettings;

    return {
      id: doc.ref.id,
      calorieLimit: result.calorieLimit,
      email: result.email,
    };
  }

  async updateCalorieLimit(
    userId: string,
    calorieLimit: number,
  ): Promise<TrackerSettings & FirestoreDocument> {
    const doc = await this.getOrCreateFoodDocument(userId);

    await doc.ref.update({ calorieLimit: calorieLimit });
    const result = (
      await this.getOrCreateFoodDocument(userId)
    ).data() as TrackerSettings;

    return {
      id: doc.ref.id,
      calorieLimit: result.calorieLimit,
      email: result.email,
    };
  }

  async trackerSettings(userId: string): Promise<TrackerSettings> {
    const result = (
      await this.getOrCreateFoodDocument(userId)
    ).data() as TrackerSettings;
    return result;
  }

  async allEntries(): Promise<UserEntries[]> {
    const all = await this.allFoodEntries().get();
    const data = all.docs.map(async (doc) => {
      const docData = doc.data();
      const entries = await this.foodEntriesForUser(docData.userId);

      return {
        settings: {
          userId: docData.userId,
          calorieLimit: docData.calorieLimit,
          email: docData.email,
        },
        foodEntries: entries,
      };
    });

    return Promise.all(data);
  }

  private mapFoodEntries(
    entries: firestore.QuerySnapshot<firestore.DocumentData>,
  ): (FoodEntry & FirestoreDocument)[] {
    const result = entries.docs.map((doc) => {
      return this.mapFoodEntry(doc);
    });
    return result;
  }

  private mapFoodEntry(
    doc: firestore.DocumentData,
  ): FoodEntry & FirestoreDocument {
    const { date, name, calories, photoUrl } = doc.data();

    return {
      id: doc.id,
      date: this.timestampToDateString(date),
      name: name,
      calories: calories,
      photoUrl: photoUrl,
    };
  }

  private timestampToDateString(time: firestore.Timestamp): string {
    return time.toDate().toISOString().slice(0, -5) + 'Z';
  }

  private async getFoodEntries(
    userId: string,
  ): Promise<
    FirebaseFirestore.CollectionReference<FirebaseFirestore.DocumentData>
  > {
    const doc = await this.getOrCreateFoodDocument(userId);
    return doc.ref.collection('foodEntriesForUser');
  }

  private async getOrCreateFoodDocument(
    userId: string,
  ): Promise<
    FirebaseFirestore.DocumentSnapshot<FirebaseFirestore.DocumentData>
  > {
    const query = await this.queryForFoodEntries(userId);

    if (query.empty) {
      const ref = await this.allFoodEntries().add({ userId });
      return await ref.get();
    } else {
      return query.docs[0];
    }
  }

  private async queryForFoodEntries(
    userId: string,
  ): Promise<firestore.QuerySnapshot<firestore.DocumentData>> {
    const query = await this.allFoodEntries()
      .where('userId', '==', userId)
      .limit(1)
      .get();
    return query;
  }

  private allFoodEntries(): FirebaseFirestore.CollectionReference<FirebaseFirestore.DocumentData> {
    return this.firebaseService.db.collection('foodEntries');
  }

  private deletePhoto(photoUrl: string) {
    const bucket = this.firebaseService.storage.bucket(
      this.firebaseService.bucketName,
    );
    const part = photoUrl.split('?')[0];
    const imageName = part.slice(-36);

    bucket.deleteFiles({ prefix: imageName });
  }
}
