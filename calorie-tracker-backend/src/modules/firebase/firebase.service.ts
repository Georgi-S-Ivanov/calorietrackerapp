import { Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { FirebaseConfig } from './firebase-config';

@Injectable()
export class FirebaseService {
  private _app: admin.app.App;
  private _db: FirebaseFirestore.Firestore;
  private _bucketName: string;

  constructor(config: FirebaseConfig) {
    if (!admin.apps.length) {
      this._app = admin.initializeApp({
        credential: admin.credential.cert(config.all),
      });
      this._db = admin.firestore(this._app);
    } else {
      this._app = admin.app();
      this._db = admin.firestore(this._app);
    }

    this._bucketName = config.storageBucket;
  }

  get db(): FirebaseFirestore.Firestore {
    return this._db;
  }

  get auth(): admin.auth.Auth {
    return this._app.auth();
  }

  get storage(): admin.storage.Storage {
    return this._app.storage();
  }

  get bucketName(): string {
    return this._bucketName;
  }
}
