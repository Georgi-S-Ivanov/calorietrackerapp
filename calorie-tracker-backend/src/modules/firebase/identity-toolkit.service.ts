import { HttpService } from '@nestjs/axios';
import { HttpException, Injectable } from '@nestjs/common';
import { lastValueFrom, map, Observable } from 'rxjs';
import { NewFirebaseUser } from './dto/new-firebase-user';
import { SignedInFirebaseUser } from './dto/signed-in-firebase-user';
import { FirebaseConfig } from './firebase-config';

@Injectable()
export class IdentityToolkitService {
  readonly identityToolkitUrl: string;
  constructor(
    private httpService: HttpService,
    private firebaseConfig: FirebaseConfig,
  ) {
    this.identityToolkitUrl = firebaseConfig.identityToolkitUrl;
  }

  async signUpUser(email: string, password: string): Promise<NewFirebaseUser> {
    const url =
      this.identityToolkitUrl +
      'accounts:signUp?key=' +
      this.firebaseConfig.webApiKey;

    const obs = this.httpService
      .post(url, {
        email: email,
        password: password,
        returnSecureToken: true,
      })
      .pipe(map((r) => r.data));

    return this.getResult(obs);
  }

  async signInUser(
    email: string,
    password: string,
  ): Promise<SignedInFirebaseUser> {
    const url =
      this.identityToolkitUrl +
      'accounts:signInWithPassword?key=' +
      this.firebaseConfig.webApiKey;
    const obs = this.httpService
      .post(url, {
        email: email,
        password: password,
        returnSecureToken: true,
      })
      .pipe(map((r) => r.data));

    return this.getResult(obs);
  }

  private async getResult(obs: Observable<any>): Promise<any> {
    try {
      const result = await lastValueFrom(obs);
      return result;
    } catch (e: any) {
      const { code, errors } = e.response.data.error;
      const error = new HttpException(
        {
          errors: errors,
          statusCode: code,
        },
        code,
      );
      throw error;
    }
  }
}
