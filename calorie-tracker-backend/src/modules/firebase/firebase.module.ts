import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { FirebaseConfig } from './firebase-config';
import { FirebaseService } from './firebase.service';
import { IdentityToolkitService } from './identity-toolkit.service';
import { CalorieTrackerService } from './calorie-tracker.service';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
    }),
  ],
  providers: [
    IdentityToolkitService,
    FirebaseConfig,
    FirebaseService,
    CalorieTrackerService,
  ],
  exports: [
    IdentityToolkitService,
    FirebaseConfig,
    FirebaseService,
    CalorieTrackerService,
  ],
})
export class FirebaseModule {}
