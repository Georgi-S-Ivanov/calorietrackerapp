import { NewFirebaseUser } from './new-firebase-user';

export class SignedInFirebaseUser extends NewFirebaseUser {
  readonly displayName!: string;
  readonly registered!: boolean;
}
