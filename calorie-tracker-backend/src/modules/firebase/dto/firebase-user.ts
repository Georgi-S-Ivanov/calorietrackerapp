import { Role } from './../../../enums/role.enum';

export class FirebaseUser {
  readonly uid!: string;
  readonly roles!: Role[];
  readonly disabled!: boolean;
  readonly email!: string;
  readonly emailVerified!: boolean;
}
