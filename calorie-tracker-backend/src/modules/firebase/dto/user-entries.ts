import { FoodEntry } from 'src/modules/tracker/dto/food-entry.dto';
import { TrackerSettings } from 'src/modules/tracker/dto/tracker-settings.dto';
import { FirestoreDocument } from '../interfaces/firestore-document';

export class UserEntries {
  settings!: TrackerSettings;
  foodEntries!: (FoodEntry & FirestoreDocument)[];
}
