import { Injectable } from '@nestjs/common';
import * as serviceAccount from '../../../firebase.config.json';

@Injectable()
export class FirebaseConfig {
  get all(): any {
    return serviceAccount;
  }

  get type(): string {
    return serviceAccount.type;
  }

  get projectId(): string {
    return serviceAccount.project_id;
  }

  get privateKeyId(): string {
    return serviceAccount.private_key_id;
  }

  get privateKey(): string {
    return serviceAccount.private_key;
  }

  get clientEmail(): string {
    return serviceAccount.client_email;
  }

  get clientId(): string {
    return serviceAccount.client_id;
  }

  get authUri(): string {
    return serviceAccount.auth_uri;
  }

  get tokenUri(): string {
    return serviceAccount.token_uri;
  }

  get authProviderX509CertUrl(): string {
    return serviceAccount.auth_provider_x509_cert_url;
  }

  get clientX509CertUrl(): string {
    return serviceAccount.client_x509_cert_url;
  }

  get webApiKey(): string {
    return serviceAccount.web_api_key;
  }

  get identityToolkitUrl(): string {
    return serviceAccount.identity_toolkit_url;
  }

  get storageBucket(): string {
    return serviceAccount.storage_bucket;
  }
}
