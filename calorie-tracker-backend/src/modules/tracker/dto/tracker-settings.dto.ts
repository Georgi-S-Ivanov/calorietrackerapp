import { IsEmail, IsInt } from 'class-validator';

export class TrackerSettings {
  @IsInt()
  calorieLimit!: number;

  @IsEmail()
  email!: string;
}
