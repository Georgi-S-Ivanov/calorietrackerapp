import { IsISO8601 } from 'class-validator';

export class FilterEntriesQuery {
  @IsISO8601()
  beginDate!: string;

  @IsISO8601()
  endDate!: string;
}
