import { IsInt, IsISO8601, IsOptional, IsString, IsUrl } from 'class-validator';

export class FoodEntry {
  @IsISO8601({ strict: true })
  date!: string;

  @IsString()
  name!: string;

  @IsInt()
  calories!: number;

  @IsOptional()
  @IsUrl()
  photoUrl?: string;
}
