import { Body, Controller, Get, Patch, Post, Query } from '@nestjs/common';
import { CalorieTrackerService } from '../firebase/calorie-tracker.service';
import { FilterEntriesQuery } from './dto/filter-entries-query';
import { FoodEntry } from './dto/food-entry.dto';
import { TrackerSettings } from './dto/tracker-settings.dto';
import { User } from '../../decorators/user.decorator';

@Controller('tracker')
export class TrackerController {
  constructor(private calorieTracker: CalorieTrackerService) {}

  @Post('foodentries')
  async addFoodEntry(@User() user: any, @Body() entry: FoodEntry) {
    return this.calorieTracker.createEntry(user.uid, entry);
  }

  @Get('foodentries')
  async entriesForUser(@User() user: any) {
    return {
      foodEntries: await this.calorieTracker.foodEntriesForUser(user.uid),
      settings: await this.calorieTracker.trackerSettings(user.uid),
    };
  }

  @Get('foodentries/filter')
  async filterEntries(@User() user: any, @Query() params: FilterEntriesQuery) {
    return this.calorieTracker.filterFoodEntries(user.uid, params);
  }

  @Patch('settings')
  async setCalorieLimit(
    @User() user: any,
    @Body() settings: Exclude<TrackerSettings, 'email'>,
  ) {
    return this.calorieTracker.updateCalorieLimit(
      user.uid,
      settings.calorieLimit,
    );
  }
}
