import { Module } from '@nestjs/common';
import { FirebaseModule } from '../firebase/firebase.module';
import { TrackerController } from './tracker.controller';

@Module({
  imports: [FirebaseModule],
  controllers: [TrackerController],
})
export class TrackerModule {}
