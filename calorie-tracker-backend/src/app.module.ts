import { Module } from '@nestjs/common';
import { FirebaseModule } from './modules/firebase/firebase.module';
import { UserModule } from './modules/user/user.module';
import { TrackerModule } from './modules/tracker/tracker.module';
import { APP_GUARD } from '@nestjs/core';
import { FirebaseAuthGuard } from './guards/firebase-auth.guard';
import { FirebaseAuthStrategy } from './strategies/firebase-auth.strategy';
import { RolesGuard } from './guards/roles.guard';
import { AdminModule } from './modules/admin/admin.module';

@Module({
  imports: [FirebaseModule, UserModule, TrackerModule, AdminModule],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: FirebaseAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
    FirebaseAuthStrategy,
  ],
})
export class AppModule {}
