import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, ExtractJwt } from 'passport-firebase-jwt';
import { FirebaseService } from './../modules/firebase/firebase.service';

@Injectable()
export class FirebaseAuthStrategy extends PassportStrategy(
  Strategy,
  'firebase-auth',
) {
  constructor(private firebaseService: FirebaseService) {
    super({ jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken() });
  }

  async validate(token: string) {
    try {
      const decodedToken = await this.firebaseService.auth.verifyIdToken(
        token,
        true,
      );
      return decodedToken;
    } catch (error: any) {
      throw new UnauthorizedException(error.message);
    }
  }
}
