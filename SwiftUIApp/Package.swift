// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Calorie Tracker",
    platforms: [.iOS(.v15)],
    products: [
        .library(name: "App", targets: ["App"]),
        .library(name: "SignIn", targets: ["SignIn"])
    ],
    dependencies: [
        .package(
            url: "https://github.com/pointfreeco/swift-composable-architecture",
            from: "0.29.0"
        ),
        .package(
            url: "https://github.com/apple/swift-collections.git",
            .upToNextMajor(from: "1.0.0") // or `.upToNextMinor
        )
    ],
    targets: [
        .target(
            name: "AddFoodEntry",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "DesignSystem",
                "Models",
                "Networking",
                "PhotoPicker",
                "SwiftUIHelpers",
                "Utility"
            ]
        ),
        .target(
            name: "Admin",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "AddFoodEntry",
                "DesignSystem",
                "Models",
                "Networking",
                "PhotoPicker",
                "SwiftUIHelpers",
                "Utility"
            ]
        ),
        .target(
            name: "App",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "Admin",
                "Models",
                "Networking",
                "PhotoPicker",
                "Profile",
                "SignIn",
                "SignUp",
                "Tracker",
                "Utility"
            ]
        ),
        .target(
            name: "DesignSystem",
            dependencies: [
                "Models"
            ]
        ),
        .target(
            name: "Mocks",
            dependencies: [
                .product(
                    name: "Collections",
                    package: "swift-collections"
                ),
                "Models",
                "Networking"
            ]
        ),
        .target(name: "Models"),
        .target(
            name: "Networking",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "Models"
            ]
        ),
        .target(
            name: "PhotoPicker"
        ),
        .target(
            name: "Profile",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "DesignSystem",
                "Models"
            ]
        ),
        .target(
            name: "SignIn",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "DesignSystem",
                "Models",
                "Networking",
                "SwiftUIHelpers",
                "Utility"
            ]
        ),
        .target(
            name: "SignUp",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "DesignSystem",
                "Models",
                "Networking",
                "SwiftUIHelpers",
                "Utility"
            ]
        ),
        .target(name: "SwiftUIHelpers"),
        .target(
            name: "Tracker",
            dependencies: [
                .product(
                    name: "ComposableArchitecture",
                    package: "swift-composable-architecture"
                ),
                "AddFoodEntry",
                "DesignSystem",
                "Models",
                "Networking",
                "PhotoPicker",
                "SwiftUIHelpers",
                "Utility"
            ]
        ),
        .target(name: "Utility", dependencies: [
            "Models"
        ])
    ]
)

// MARK: - Tests

package.targets.append(contentsOf: [
    .testTarget(name: "SignUpTests", dependencies: [
        .product(
            name: "ComposableArchitecture",
            package: "swift-composable-architecture"
        ),
        "Mocks",
        "Models",
        "Networking",
        "SignUp"
    ])
])
