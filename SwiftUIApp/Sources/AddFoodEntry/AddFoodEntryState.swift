import ComposableArchitecture
import Models
import Utility
import PhotoPicker
import UIKit

public struct AddFoodEntryState: Equatable {
    @BindableState public var name = ""
    @BindableState public var calories = 0
    @BindableState public var dateEaten: Date = StaticEnvironment.dateNow()
    @BindableState public var showPicker: AddFoodEntryRoute?
    public var pickImage: UIImage?
    public var photoUrl: String?
    var uploadedPhotoUrl: String?
    var canProceed = false

    public var subtitle: String?
    public var showProgress: Bool = false
    public var nameError: String?
    public var caloriesError: String?
    public var dateEatenError: String?
    public var imageError: String?
    public var apiError: ApiError?

    public var successMessage: String?

    public init() {

    }

    mutating func clearErrors() {
        nameError = nil
        caloriesError = nil
        dateEatenError = nil
        imageError = nil
        apiError = nil
    }

    public mutating func prepareForNextEntry() {
        name = ""
        calories = 0
        dateEaten = StaticEnvironment.dateNow()
        pickImage = nil
        photoUrl = nil
        uploadedPhotoUrl = nil
    }

    func imageShouldBeUploaded() -> Bool {
        pickImage != nil && uploadedPhotoUrl == nil
    }
}

public extension AddFoodEntryState {
    var postEntry: PostFoodEntry? {
        guard canProceed else {
            return nil
        }

        return PostFoodEntry(
            date: dateEaten,
            name: name,
            calories: calories,
            photoUrl: uploadedPhotoUrl ?? photoUrl
        )
    }

    func updateEntry(id: String) -> UpdateFoodEntry? {
        guard canProceed else {
            return nil
        }

        return UpdateFoodEntry(
            id: id,
            date: dateEaten,
            name: name,
            calories: calories,
            photoUrl: uploadedPhotoUrl ?? photoUrl
        )
    }
}
