import SwiftUI
import ComposableArchitecture
import DesignSystem
import PhotoPicker

public struct AddFoodEntryView: View {

    let title: String
    let store: Store<AddFoodEntryState, AddFoodEntryAction>
    @ObservedObject var viewStore: ViewStore<AddFoodEntryState, AddFoodEntryAction>

    public init(title: String, store: Store<AddFoodEntryState, AddFoodEntryAction>) {
        self.title = title
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        VStack {
            Text(title).font(.title2)
            if let subtitle = viewStore.subtitle {
                Text(subtitle).font(.subheadline)
            }

            FormText(
                "Name",
                textBinding: viewStore.binding(\.$name),
                errorMessage: viewStore.nameError
            )
            .disabled(viewStore.showProgress)

            FormNumber(
                "Calories",
                numberBinding: viewStore.binding(\.$calories),
                errorMessage: viewStore.caloriesError
            )
            .disabled(viewStore.showProgress)

            FormDatePicker(
                title: "Eaten On",
                components: [.date, .hourAndMinute],
                dateBinding: viewStore.binding(\.$dateEaten),
                errorMessage: viewStore.dateEatenError
            )
            .disabled(viewStore.showProgress)

            ErrorMessage(viewStore.apiError)

            HStack {
                Button {
                    // Withtout closing keyboard here
                    // we get this error:
                    // AttributeGraph: cycle detected through attribute
                    closeKeyboard()
                    viewStore.send(.addEntry)
                } label: {
                    Text("Add Entry")
                }
                .disabled(viewStore.showProgress)
                .overlay(alignment: .leading) {
                    NetworkActivity(show: viewStore.state.showProgress)
                        .offset(x: -Space.large)
                }

                Button {
                    viewStore.send(.setNavigationRoute(.showPicker))
                } label: {
                    Image(systemName: "photo")
                }
                .disabled(viewStore.showProgress)
                .padding([.leading], Space.large)
                .sheet(
                    item: viewStore.binding(\.$showPicker)
                ) { _ in
                    PhotoPicker(
                        onPicked: { fetchResult in
                            viewStore.send(.pickImage(fetchResult))
                        },
                        onDismiss: {
                            viewStore.send(.setNavigationRoute(nil))
                        }
                    )
                }

            }

            if let image = viewStore.state.pickImage {
                Image(uiImage: image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(maxWidth: 320, maxHeight: 240)
            } else if let photoUrl = viewStore.photoUrl {
                AsyncImage(
                    url: URL(string: photoUrl),
                    content: { image in
                        image
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 320, maxHeight: 240)
                    }
                ) {
                    Colors.gray
                    .frame(maxWidth: 320, maxHeight: 240)
                }
            }

            if let successMessage = viewStore.successMessage {
                SuccessMessage(successMessage)
            }

            Spacer()
        }
    }

    func closeKeyboard() {
        UIApplication.shared.sendAction(
            #selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil
        )
    }
}

#if DEBUG

struct AddFoodEntryView_Previews: PreviewProvider {
    static var previews: some View {
        return AddFoodEntryView(
            title: "Add Entry",
            store: .init(
                initialState: .init(),
                reducer: addFoodEntryReducer,
                environment: .noop
            )
        )

    }
}

#endif
