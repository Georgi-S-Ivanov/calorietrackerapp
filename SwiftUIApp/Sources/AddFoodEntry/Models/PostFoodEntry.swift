import Foundation

public struct PostFoodEntry: Codable, Equatable {
    let date: Date
    let name: String
    let calories: Int
    let photoUrl: String?

    init(
        date: Date,
        name: String,
        calories: Int,
        photoUrl: String?
    ) {
        self.date = date
        self.name = name
        self.calories = calories
        self.photoUrl = photoUrl
    }
}
