import Models
import Foundation

public struct UpdateFoodEntry: Codable, Equatable {
    let id: String
    let date: Date
    let name: String
    let calories: Int
    let photoUrl: String?

    init(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        photoUrl: String?
    ) {
        self.id = id
        self.date = date
        self.name = name
        self.calories = calories
        self.photoUrl = photoUrl
    }

    public init(
        _ entry: FoodEntry
    ) {
        self.id = entry.id
        self.date = entry.date
        self.name = entry.name
        self.calories = entry.calories
        self.photoUrl = entry.photoUrl
    }
}

public extension UpdateFoodEntry {
    var toApiModel: FoodEntry {
        FoodEntry(
            id: id,
            date: date,
            name: name,
            calories: calories,
            photoUrl: photoUrl
        )
    }
}
