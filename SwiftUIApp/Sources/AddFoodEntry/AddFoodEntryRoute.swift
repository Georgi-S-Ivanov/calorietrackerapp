import Foundation

public enum AddFoodEntryRoute: Int, Identifiable {
    case showPicker

    public var id: Int {
        return self.rawValue
    }
}
