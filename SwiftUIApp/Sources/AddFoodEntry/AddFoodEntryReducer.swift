import ComposableArchitecture

public let addFoodEntryReducer = Reducer<
    AddFoodEntryState, AddFoodEntryAction, AddFoodEntryEnvironment
> { (state, action, env) in
    switch action {
    case .binding:
        return .none

    case .addEntry:
        state.canProceed = false
        guard !state.name.isEmpty else {
            state.nameError = "Name field must not be empty."
            return .none
        }

        guard state.calories > 0 else {
            state.caloriesError = "Calories cannot be 0."
            return .none
        }

        guard state.dateEaten <= env.dateNow() else {
            state.dateEatenError = "Date cannot be in the future."
            return .none
        }

        state.clearErrors()
        state.showProgress = true

        if state.imageShouldBeUploaded(),
           let image = state.pickImage {
            return env.firebase
                .uploadImage(image)
                .catchToEffect(AddFoodEntryAction.imageUploaded)
        }

        state.canProceed = true

        return .none

    case let .addEntryResult(result):
        state.showProgress = false
        switch result {
        case .success:
            state.successMessage = "Food entry added."
            return .none
        case let .failure(error):
            state.apiError = error
            return .none
        }

    case let .pickImage(pickResult):
        return env.photoAssets.getAsset(pickResult)
            .catchToEffect(AddFoodEntryAction.retrieveImageAsset)
    case let .retrieveImageAsset(result):
        switch result {
        case let .success(image):
            state.pickImage = image
            return .none
        case let .failure(error):
            state.imageError = error.message
            return .none
        }
    case let .imageUploaded(result):
        switch result {
        case let .success(url):
            state.uploadedPhotoUrl = url.absoluteString
            return .init(value: .addEntry)
        case let .failure(error):
            state.showProgress = false
            state.imageError = error.message
            return .none
        }

    case let .setNavigationRoute(route):
        switch route {
        case .showPicker:
            state.showPicker = .showPicker
            return .none
        case .none:
            state.showPicker = nil
            return .none
        }
    }
}
.binding()
