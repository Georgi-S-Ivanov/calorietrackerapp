import ComposableArchitecture
import Models
import PhotoPicker
import UIKit
import Networking

public enum AddFoodEntryAction: BindableAction, Equatable {
    case binding(BindingAction<AddFoodEntryState>)
    case addEntry
    case addEntryResult(Result<HttpStatus, ApiError>)
    case pickImage(PhotoPickResult)
    case retrieveImageAsset(Result<UIImage, PhotoAssetsError>)
    case imageUploaded(Result<URL, FirebaseSDKError>)
    case setNavigationRoute(AddFoodEntryRoute?)
}
