import ComposableArchitecture
import Models
import Networking

public let signInReducer = Reducer<SignInState, SignInAction, SignInEnvironment> { state, action, env in
    switch action {
    case .binding:
        return .none
    case .signIn:

        guard !state.email.isEmpty else {
            state.emailError = "Email field must not be empty."
            return .none
        }

        guard !state.password.isEmpty else {
            state.passwordError = "Password field must not be empty."
            return .none
        }

        state.clearUpErrors()
        state.showProgress = true

        return env.apiClient.apiRequest(
            route: Endpoint.signIn(
                email: state.email,
                password: state.password
            ),
            as: User.self
        )
            .receive(on: env.mainQueue)
            .catchToEffect(SignInAction.userSignedIn)
    case let .setNavigation(tag):
        state.signInRoute = tag
        return .none
    case let .userSignedIn(result):
        switch result {
        case .success:
            break
        case let .failure(error):
            state.signInError = error.userErrorMessage()
        }

        state.showProgress = false
        return .none
    }
}
    .binding()
