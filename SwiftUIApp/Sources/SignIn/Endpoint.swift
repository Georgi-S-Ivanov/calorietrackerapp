import Foundation
import Models
import Utility
import Networking

enum Endpoint {
    case signIn(email: String, password: String)
}

extension Endpoint: ApiRoute {
    var path: String {
        switch self {
        case .signIn:
            return "user/signin"
        }
    }

    var method: HttpMethod {
        switch self {
        case .signIn:
            return .post
        }
    }

    var needsAuthorization: Bool {
        false
    }

    var body: Data? {
        switch self {
        case let .signIn(email, password):
            let dict = ["email": email, "password": password]
            return try? Json.encoder.encode(dict)
        }
    }
}
