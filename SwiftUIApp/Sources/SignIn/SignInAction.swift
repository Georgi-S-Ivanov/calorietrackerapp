import ComposableArchitecture
import Models

public enum SignInAction: BindableAction, Equatable {
    case binding(BindingAction<SignInState>)
    case signIn
    case userSignedIn(Result<User, ApiError>)
    case setNavigation(SignInRoute?)
}
