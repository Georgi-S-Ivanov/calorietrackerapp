import ComposableArchitecture

public struct SignInState: Equatable {
    @BindableState var email: String = ""
    @BindableState var password: String = ""
    var signInRoute: SignInRoute?
    var showProgress: Bool = false

    var emailError: String?
    var passwordError: String?
    var signInError: String?

    mutating func clearUpErrors() {
        emailError = nil
        passwordError = nil
        signInError = nil
    }

    public init() {

    }
}
