import SwiftUI
import ComposableArchitecture
import Models
import DesignSystem

public struct SignInView: View {
    let store: Store<SignInState, SignInAction>
    @ObservedObject var viewStore: ViewStore<SignInState, SignInAction>
    let signUpView: () -> AnyView

    public init(_ store: Store<SignInState, SignInAction>, signUpView: @escaping () -> AnyView) {
        self.store = store
        self.viewStore = ViewStore(store)
        self.signUpView = signUpView
    }

    public var body: some View {
        VStack {

            Text("Calorie Tracker")
                .font(.title2)
                .padding(Space.small)

            GroupBox {
                VStack {
                    FormText(
                        "Email",
                        textBinding: viewStore.binding(\.$email),
                        errorMessage: viewStore.emailError
                    )

                    FormText(
                        "Password",
                        textBinding: viewStore.binding(\.$password),
                        errorMessage: viewStore.passwordError,
                        secure: true
                    )

                    Button {
                        viewStore.send(.signIn)
                    } label: {
                        Text("Sign in")
                    }
                    .padding([.top, .bottom], Space.tiny)
                    .overlay(alignment: .leading) {
                        NetworkActivity(show: viewStore.state.showProgress)
                            .offset(x: -Space.large)
                    }

                }
            }
            .padding()

            if let signInError = viewStore.signInError {
                Text(signInError)
                    .padding(Space.small)
                    .foregroundColor(.red)
            }

            HStack {
                Text("Don't have an account?")
                NavigationLink(
                    tag: SignInRoute.signUp,
                    selection: viewStore.binding(
                        get: \.signInRoute,
                        send: SignInAction.setNavigation
                    ),
                    destination: {
                        signUpView()
                    },
                    label: {
                        Text("Sign up")
                    }
                )
            }
        }
        .padding([.bottom], Space.xlarge * 3)

        Spacer()
    }
}

#if DEBUG
import SwiftUIHelpers

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        return Preview {
            NavigationView {
                SignInView(
                    .init(
                        initialState: .init(),
                        reducer: signInReducer,
                        environment: .noop
                    ),
                    signUpView: { AnyView(Text("This is Sign Up")) }
                )
            }
        }
    }
}

#endif
