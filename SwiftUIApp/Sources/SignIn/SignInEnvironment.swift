import ComposableArchitecture
import Networking

public struct SignInEnvironment {

    public let apiClient: ApiClient
    public let mainQueue: AnySchedulerOf<DispatchQueue>

    public init(
        apiClient: ApiClient,
        mainQueue: AnySchedulerOf<DispatchQueue>
    ) {
        self.apiClient = apiClient
        self.mainQueue = mainQueue
    }
}

#if DEBUG

public extension SignInEnvironment {
    static var noop: SignInEnvironment {
        .init(
            apiClient: .noop,
            mainQueue: .immediate
        )
    }
}

#endif
