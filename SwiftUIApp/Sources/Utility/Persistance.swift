import Foundation
import Models

public enum Persistance {
    public static func save<T>(_ obj: T?, withKey key: UserDefaultsKeys) -> Bool where T: Encodable {
        guard let obj = obj else {
            UserDefaults.standard.removeObject(forKey: key)
            return false
        }

        guard let data = try? Json.encoder.encode(obj) else {
            return false
        }

        UserDefaults.standard.setValue(data, forKey: key)
        return true
    }

    public static func load<T>(forKey key: UserDefaultsKeys) -> T? where T: Decodable {
        UserDefaults.standard.data(forKey: key)
        .flatMap({ try? JSONDecoder().decode(T.self, from: $0) })
    }
}
