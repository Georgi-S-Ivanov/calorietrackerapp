import Foundation

/// Some state values have to be initialized when `Global store`
/// is created. At this time using the regular environment passed to
/// a reducer is impossible. To have these values initialized and still
/// have the ability to stub them in tests, this environment is created.
///
public enum StaticEnvironment {
    public static func dateNow() -> Date {
        Date()
    }
}
