import Foundation

public enum DateFormat {
    private static var shortFormatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "dd MMM YYYY"
        return f
    }()

    private static var longFormatter: DateFormatter = {
        let f = DateFormatter()
        f.dateFormat = "dd MMM YYYY, HH:MM:ss"
        return f
    }()

    private static var isoFormatter: ISO8601DateFormatter = {
        return ISO8601DateFormatter()
    }()

    public static func short(_ date: Date) -> String {
        return shortFormatter.string(from: date)
    }

    public static func long(_ date: Date) -> String {
        return longFormatter.string(from: date)
    }

    public static func iso(_ date: Date) -> String {
        return isoFormatter.string(from: date)
    }

    public static func stripDate(_ date: Date) -> Date {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: date)
        let result = Calendar.current.date(from: components)!
        return result
    }

    public static func startOfDay(_ date: Date) -> Date {
        Calendar.current.startOfDay(for: date)
    }

    public static func endOfDay(_ date: Date) -> Date {
        Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: date)!
    }
}
