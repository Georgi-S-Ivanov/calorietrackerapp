import Foundation

public enum Number {
    private static var formatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        return numberFormatter
    }()

    public static func format(_ value: Int) -> String {
        formatter.string(for: value) ?? "\(value)"
    }

    public static func format(_ value: Double) -> String {
        formatter.string(for: value) ?? "\(value)"
    }
}
