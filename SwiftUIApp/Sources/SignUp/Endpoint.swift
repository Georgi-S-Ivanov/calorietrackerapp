import Foundation
import Models
import Utility
import Networking

public enum Endpoint {
    case signUp(dto: SignUpDto)
}

extension Endpoint: ApiRoute {
    public var path: String {
        switch self {
        case .signUp:
            return "user/signup"
        }
    }

    public var method: HttpMethod {
        switch self {
        case .signUp:
            return .post
        }
    }

    public var needsAuthorization: Bool {
        return false
    }

    public var body: Data? {
        switch self {
        case let .signUp(dto):
            return try? Json.encoder.encode(dto)
        }
    }
}
