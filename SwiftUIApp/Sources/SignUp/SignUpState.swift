import ComposableArchitecture

public struct SignUpState: Equatable {
    @BindableState var email: String = ""
    @BindableState var password: String = ""
    @BindableState var confirmPassword: String = ""
    @BindableState var isAdmin: Bool = false
    var showProgress: Bool = false

    var emailError: String?
    var passwordError: String?
    var signUpRequestError: String?

    public init() {

    }
}
