import Models
import Networking
import ComposableArchitecture

public struct SignUpEnvironment {
    public let apiClient: ApiClient
    public let saveUser: (User?) -> Bool
    public let mainQueue: AnySchedulerOf<DispatchQueue>

    public init(
        apiClient: ApiClient,
        saveUser: @escaping (User?) -> Bool,
        mainQueue: AnySchedulerOf<DispatchQueue>
    ) {
        self.apiClient = apiClient
        self.saveUser = saveUser
        self.mainQueue = mainQueue
    }
}

#if DEBUG

public extension SignUpEnvironment {
    static var noop: SignUpEnvironment {
        .init(
            apiClient: .noop,
            saveUser: { _ in false },
            mainQueue: .immediate
        )
    }
}

#endif
