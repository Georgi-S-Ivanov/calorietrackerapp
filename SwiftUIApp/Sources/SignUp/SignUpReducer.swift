import ComposableArchitecture
import Models
import Combine

public let signUpReducer = Reducer<SignUpState, SignUpAction, SignUpEnvironment> { (state, action, env) in
    switch action {
    case .binding:
        return .none
    case .signUp:
        var canSignUp = true
        if state.email.split(separator: "@").count != 2 {
            canSignUp = false
            state.emailError = "Please enter valid email."
        } else {
            state.emailError = nil
        }

        guard !state.password.isEmpty || !state.confirmPassword.isEmpty else {
            state.passwordError = "Please enter a password."
            return .none
        }

        guard state.password.count >= 6 else {
            state.passwordError = "Password must be at least 6 symbols."
            return .none
        }

        if state.password != state.confirmPassword {
            canSignUp = false
            state.passwordError = "Passwords do not match."
        } else {
            state.passwordError = nil
        }

        guard canSignUp else {
            return .none
        }

        let dto = SignUpDto(
            email: state.email,
            password: state.password,
            confirmPassword: state.confirmPassword,
            roles: [state.isAdmin ? UserRole.admin : UserRole.user]
        )

        state.showProgress = true
        return env.apiClient.apiRequest(
            route: Endpoint.signUp(dto: dto),
            as: User.self
        )
            .receive(on: env.mainQueue)
            .catchToEffect(SignUpAction.signUpRequest)
    case let .signUpRequest(result):
        switch result {
        case let .success(user):
            _ = env.saveUser(user)
        case let .failure(signUpError):
            state.signUpRequestError = signUpError.userErrorMessage()
        }

        state.showProgress = false
        return .none
    }
}
.binding()
