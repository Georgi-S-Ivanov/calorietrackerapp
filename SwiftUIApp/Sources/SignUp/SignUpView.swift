import SwiftUI
import ComposableArchitecture
import DesignSystem

public struct SignUpView: View {
    let store: Store<SignUpState, SignUpAction>
    @ObservedObject var viewStore: ViewStore<SignUpState, SignUpAction>

    public init(_ store: Store<SignUpState, SignUpAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        VStack {
            Text("Sign up")
            .font(.largeTitle)
            .padding([.bottom], Space.xlarge)

            GroupBox {
                createFormViews()
            }
            .padding(Space.small)

            Button("Sign up") {
                viewStore.send(.signUp)
            }
            .disabled(viewStore.showProgress)
            .overlay(alignment: .leading) {
                NetworkActivity(show: viewStore.state.showProgress)
                    .offset(x: -Space.large)
            }

            if let signInError = viewStore.signUpRequestError {
                Text(signInError)
                .padding(Space.small)
                .foregroundColor(.red)
            }

            Spacer()
        }
    }

    @ViewBuilder func createFormViews() -> some View {
        VStack {
            FormText(
                "Email",
                textBinding: viewStore.binding(\.$email),
                errorMessage: viewStore.emailError
            )

            FormText(
                "Password",
                textBinding: viewStore.binding(\.$password),
                errorMessage: viewStore.passwordError,
                secure: true
            )

            FormText(
                "Confirm Password",
                textBinding: viewStore.binding(\.$confirmPassword),
                errorMessage: viewStore.passwordError,
                secure: true
            )

            Toggle("Is Admin", isOn: viewStore.binding(\.$isAdmin))
            .padding([.leading, .trailing], Space.xlarge)
            .padding([.top, .bottom], Space.base)
        }
    }
}

#if DEBUG
import SwiftUIHelpers

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        return Preview {
            SignUpView(
                .init(
                    initialState: .init(),
                    reducer: signUpReducer,
                    environment: .noop
                )
            )
        }
    }
}

#endif
