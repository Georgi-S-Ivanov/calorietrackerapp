import Models

public struct SignUpDto: Codable {
    let email: String
    let password: String
    let confirmPassword: String
    let roles: [UserRole]
}
