import ComposableArchitecture
import Models

public enum SignUpAction: BindableAction, Equatable {
    case binding(BindingAction<SignUpState>)
    case signUp
    case signUpRequest(Result<User, ApiError>)
}
