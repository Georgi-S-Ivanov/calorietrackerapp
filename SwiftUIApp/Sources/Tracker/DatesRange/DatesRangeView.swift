import SwiftUI
import ComposableArchitecture
import DesignSystem

public struct DatesRangeView: View {
    let store: Store<DatesRangeState, DatesRangeAction>
    @ObservedObject var viewStore: ViewStore<DatesRangeState, DatesRangeAction>

    public init(_ store: Store<DatesRangeState, DatesRangeAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        VStack(spacing: Space.base) {
            FormDatePicker(
                title: "Start",
                components: [.date],
                dateBinding: viewStore.binding(\.$start),
                errorMessage: viewStore.startError
            )

            FormDatePicker(
                title: "End",
                components: [.date],
                dateBinding: viewStore.binding(\.$end),
                errorMessage: viewStore.endError
            )

            if viewStore.filterActive {
                Button {
                    viewStore.send(.deactivate)
                } label: {
                    Image(systemName: "xmark.circle")
                }
            } else {
                Button {
                    viewStore.send(.validate)
                } label: {
                    Text("Apply")
                }
            }
            Spacer()
        }
        .padding([.leading, .trailing], Space.xlarge)
        .padding([.top], Space.xlarge)
    }
}

#if DEBUG

struct DatesRangeView_Previews: PreviewProvider {
    static var previews: some View {
        return DatesRangeView(
            .init(
                initialState: .init(),
                reducer: datesRangeReducer,
                environment: .noop
            )
        )

    }
}

#endif
