import Foundation
import Utility

public struct DateFilterRange: Equatable {
    let start: Date
    let end: Date
}
