import ComposableArchitecture
import Utility

public struct DatesRangeState: Equatable {

    @BindableState var start: Date
    @BindableState var end: Date

    var filterActive: Bool
    var startError: String?
    var endError: String?

    init() {
        start = StaticEnvironment.dateNow()
        end = StaticEnvironment.dateNow()
        filterActive = false
    }
}

public extension DatesRangeState {
    var filterImageName: String {
        filterActive ?
        "line.3.horizontal.decrease.circle.fill" :
        "line.3.horizontal.decrease.circle"
    }

    mutating func clearDatePickErrors() {
        self.startError = nil
        self.endError = nil
    }
}
