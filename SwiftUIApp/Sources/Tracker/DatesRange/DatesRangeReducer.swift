import ComposableArchitecture
import Utility

let datesRangeReducer = Reducer<
    DatesRangeState, DatesRangeAction, TrackerEnvironment
> { (state, action, _) in
    switch action {
    case .binding(\.$start):
        state.filterActive = false
        if state.start > state.end {
            state.startError = "Start date cannot be after end date."
        } else {
            state.clearDatePickErrors()
        }
        return .none
    case .binding(\.$end):
        state.filterActive = false
        if state.start > state.end {
            state.endError = "End date cannot be before start date."
        } else {
            state.clearDatePickErrors()
        }
        return .none
    case .binding:
        return .none
    case .validate:
        guard state.startError == nil &&
        state.endError == nil else {
            return .none
        }

        let dto = DateFilterRange(
            start: DateFormat.startOfDay(state.start),
            end: DateFormat.endOfDay(state.end)
        )

        state.filterActive = true
        return .init(
            value: .apply(dto)
        )
    case .apply:
        return .none
    case .deactivate:
        state.filterActive = false
        return .none
    }
}
.binding()
