import Foundation
import ComposableArchitecture

public enum DatesRangeAction: BindableAction, Equatable {
    case binding(BindingAction<DatesRangeState>)
    case validate
    case apply(DateFilterRange)
    case deactivate
}
