import SwiftUI
import ComposableArchitecture
import Utility
import DesignSystem
import AddFoodEntry

public struct TrackerView: View {
    let store: Store<TrackerState, TrackerAction>
    @ObservedObject var viewStore: ViewStore<TrackerState, TrackerAction>

    public init(_ store: Store<TrackerState, TrackerAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        NavigationView {
            contentOrEmpty()
            .overlay(alignment: .center) {
                NetworkActivity(
                    show: viewStore.state.showProgress,
                    scale: 1.5
                )
                .offset(y: -Space.xlarge)
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button(action: {
                        viewStore.send(.setNavigationRoute(TrackerRoute.addFoodEntry))
                    }) {
                        Image(systemName: "plus.circle")
                            .resizable()
                            .frame(width: Space.base, height: Space.base)
                            .foregroundColor(.blue)
                    }
                }

                ToolbarItem(placement: .navigationBarLeading) {
                    entriesFilterButton()
                }

                ToolbarItem {
                    NavigationLink(
                        destination: AddFoodEntryView(
                            title: "Add Entry",
                            store: store.scope(
                                state: \.entryToAdd,
                                action: TrackerAction.addFoodEntry
                            )
                        ),
                        tag: TrackerRoute.addFoodEntry,
                        selection: viewStore.binding(
                            get: { $0.navigationRoute },
                            send: TrackerAction.setNavigationRoute
                        )
                    ) { }
                }
            }
        }
    }

    @ViewBuilder func contentOrEmpty() -> some View {
        if viewStore.viewState.days.count > 0 {
            AnyView(content())
        } else {
            AnyView(
                Text(viewStore.emptyScreenMessage)
                .multilineTextAlignment(.center)
            )
            .padding([.bottom], Space.xlarge * 3)
        }
    }

    @ViewBuilder func content() -> some View {
        List(viewStore.viewState.days) { days in
            Section(
                header: Text("\(days.totalCalories) / \(viewStore.viewState.settings.calorieLimit)")
                    .foregroundColor(
                        days.totalCalories > viewStore.viewState.settings.calorieLimit
                        ? Color.red
                        : Color.green
                    )
            ) {
                ForEach(days.entries) { entry in
                    NavigationLink(
                        destination: FoodEntryDetailsView(entry)
                    ) {
                        HStack {
                            Text("\(entry.name) (\(entry.calories))")
                            Spacer()
                            Text("\(DateFormat.short(entry.date))")
                        }
                    }
                }

            }
        }
    }

    @ViewBuilder func entriesFilterButton() -> some View {
        Button(action: {
            viewStore.send(.setNavigationRoute(TrackerRoute.setEntriesFilter))
        }) {
            Image(systemName: viewStore.filterState.filterImageName)
                .resizable()
                .frame(width: Space.base, height: Space.base)
                .foregroundColor(.blue)
        }
        .sheet(
            isPresented: .init(
                get: { viewStore.navigationRoute == .setEntriesFilter },
                set: { present, _ in
                    let r = present ? TrackerRoute.setEntriesFilter : nil
                    viewStore.send(.setNavigationRoute(r))
                }
            ),
            content: {
                DatesRangeView(
                    store.scope(
                        state: \.filterState,
                        action: TrackerAction.foodEntriesFilter
                    )
                )
            }
        )
    }
}

#if DEBUG

struct TrackerView_Previews: PreviewProvider {
    static var previews: some View {
        return TrackerView(
            .init(
                initialState: .init(),
                reducer: trackerReducer,
                environment: .noop
            )
        )

    }
}

#endif
