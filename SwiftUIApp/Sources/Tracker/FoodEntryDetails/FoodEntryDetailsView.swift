import SwiftUI
import Models
import Utility
import DesignSystem

struct FoodEntryDetailsView: View {

    let entry: FoodEntry

    init(_ entry: FoodEntry) {
        self.entry = entry
    }

    var body: some View {
        VStack(spacing: Space.base) {
            HStack {
                Text("Name:")
                Spacer()
                Text(entry.name)
            }

            HStack {
                Text("Calories:")
                Spacer()
                Text("\(entry.calories)")
            }

            HStack {
                Text("On:")
                Spacer()
                Text("\(DateFormat.long(entry.date))")
            }

            if let photoUrl = entry.photoUrl {
                AsyncImage(
                    url: URL(string: photoUrl),
                    content: { image in
                        image
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(maxWidth: 320, maxHeight: 240)
                    }
                ) {
                    Colors.gray
                    .frame(maxWidth: 320, maxHeight: 240)
                }
            }

            Spacer()
        }
        .padding([.leading, .trailing], Space.xlarge)
        .padding([.top], Space.xlarge)
    }
}
