import Foundation
import Models
import Utility
import AddFoodEntry
import Networking

enum Endpoint {
    case foodEntries
    case addEntry(PostFoodEntry)
    case filterEntries(DateFilterRange)
}

extension Endpoint: ApiRoute {
    var path: String {
        switch self {
        case .foodEntries, .addEntry:
            return "tracker/foodentries"
        case let .filterEntries(range):
            let start = DateFormat.iso(range.start)
            let end = DateFormat.iso(range.end)
            return "tracker/foodentries/filter?beginDate=\(start)&endDate=\(end)"
        }
    }

    var method: HttpMethod {
        switch self {
        case .foodEntries,
            .filterEntries:
            return .get
        case .addEntry:
            return .post
        }
    }

    var needsAuthorization: Bool {
        true
    }

    var body: Data? {
        switch self {
        case .foodEntries,
            .filterEntries:
            return nil
        case let .addEntry(dto):
            return try? Json.encoder.encode(dto)
        }
    }
}
