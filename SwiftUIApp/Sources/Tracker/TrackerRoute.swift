public enum TrackerRoute: String, Equatable {
    case addFoodEntry
    case setEntriesFilter
}
