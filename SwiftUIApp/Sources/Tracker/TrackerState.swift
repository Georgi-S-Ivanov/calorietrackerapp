import Foundation
import Models
import AddFoodEntry
import Utility

public struct TrackerState: Equatable {
    public var viewState = TrackerViewState()
    var entryToAdd = AddFoodEntryState()
    var filterState = DatesRangeState()

    var showProgress: Bool = false
    var navigationRoute: TrackerRoute?
    var errorMessage: String?

    public init() {

    }
}

extension TrackerState {
    var emptyScreenMessage: String {
        guard showProgress == false else {
            return ""
        }

        if filterState.filterActive {
            return """
No results for dates between
\(DateFormat.short(filterState.start))
and
\(DateFormat.short(filterState.end))
"""
        } else {
            return "No food entries, yet."
        }
    }
}
