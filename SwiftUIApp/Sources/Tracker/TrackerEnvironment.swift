import ComposableArchitecture
import Networking
import PhotoPicker
import Models

public struct TrackerEnvironment {

    let apiClient: ApiClient
    let mainQueue: AnySchedulerOf<DispatchQueue>
    let dateNow: () -> Date
    let photoAssets: PhotoAssetsServiceType
    let firebase: FirebaseSDK

    public init(
        apiClient: ApiClient,
        mainQueue: AnySchedulerOf<DispatchQueue>,
        dateNow: @escaping () -> Date,
        photoAssets: PhotoAssetsServiceType,
        firebase: FirebaseSDK
    ) {
        self.apiClient = apiClient
        self.mainQueue = mainQueue
        self.dateNow = dateNow
        self.photoAssets = photoAssets
        self.firebase = firebase
    }
}

#if DEBUG

public extension TrackerEnvironment {
    static var noop: TrackerEnvironment {
        .init(
            apiClient: .noop,
            mainQueue: .immediate,
            dateNow: { Date() },
            photoAssets: PhotoAssetsServiceNoop(),
            firebase: FirebaseSDKNoop()
        )
    }
}

#endif
