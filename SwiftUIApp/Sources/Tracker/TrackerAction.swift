import Models
import AddFoodEntry

public enum TrackerAction: Equatable {
    case getFoodEntries
    case foodEntriesResult(Result<UserFoodEntries, ApiError>)
    case setNavigationRoute(TrackerRoute?)
    case addFoodEntry(AddFoodEntryAction)
    case foodEntriesFilter(DatesRangeAction)
    case foodEntriesFilterResult(Result<[FoodEntry], ApiError>)
}
