import ComposableArchitecture
import Foundation
import Models
import Utility

public struct TrackerViewState: Equatable {
    struct EntriesForDay: Equatable, Identifiable {
        var id: String {
            assert(entries.count > 0)
            return entries[0].id
        }

        let totalCalories: Int
        let entries: [FoodEntry]
    }

    private(set) var days: [EntriesForDay]
    public let settings: TrackerSettings

    init() {
        self.days = []
        self.settings = TrackerSettings()
    }

    init(userFoodEntries: UserFoodEntries) {
        settings = userFoodEntries.settings
        days = groupEntries(userFoodEntries.foodEntries)
    }

    mutating func updateDays(_ entries: [FoodEntry]) {
        days = groupEntries(entries)
    }

    mutating func clearDays() {
        days = []
    }
}

private func groupEntries(_ entries: [FoodEntry]) -> [TrackerViewState.EntriesForDay] {
    var caloriesForDay = 0
    var entriesForDay = [FoodEntry]()
    var grouped = [TrackerViewState.EntriesForDay]()
    var datesSoFar = Set<Date>()

    if entries.count > 0 {
        let date = DateFormat.stripDate(entries[0].date)
        datesSoFar.insert(date)
    }

    for entry in entries {
        let date = DateFormat.stripDate(entry.date)

        if datesSoFar.contains(date) {
            caloriesForDay += entry.calories
            entriesForDay.append(entry)
        } else {
            grouped.append(
                TrackerViewState.EntriesForDay(
                    totalCalories: caloriesForDay,
                    entries: entriesForDay
                )
            )

            datesSoFar.insert(date)
            caloriesForDay = entry.calories
            entriesForDay = [entry]
        }
    }

    if entriesForDay.count > 0 {
        grouped.append(
            TrackerViewState.EntriesForDay(
                totalCalories: caloriesForDay,
                entries: entriesForDay
            )
        )
    }

    return grouped
}
