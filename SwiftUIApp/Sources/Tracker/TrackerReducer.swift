import ComposableArchitecture
import Models
import AddFoodEntry

public let trackerReducer = Reducer<
    TrackerState, TrackerAction, TrackerEnvironment
>.combine(
    addFoodEntryReducer.pullback(
        state: \.entryToAdd,
        action: /TrackerAction.addFoodEntry,
        environment: {
            AddFoodEntryEnvironment(
                apiClient: $0.apiClient,
                mainQueue: $0.mainQueue,
                dateNow: $0.dateNow,
                photoAssets: $0.photoAssets,
                firebase: $0.firebase
            )
        }
    ),
    datesRangeReducer.pullback(
        state: \.filterState,
        action: /TrackerAction.foodEntriesFilter,
        environment: { $0 }
    ),
    main
)

private let main = Reducer<
    TrackerState, TrackerAction, TrackerEnvironment
> { (state, action, env) in
    switch action {
    case .getFoodEntries:
        state.showProgress = true
        return env.apiClient.apiRequest(
            route: Endpoint.foodEntries,
            as: UserFoodEntries.self
        )
        .receive(on: env.mainQueue)
        .catchToEffect(TrackerAction.foodEntriesResult)
    case let .foodEntriesResult(result):
        state.showProgress = false
        switch result {
        case let .success(userEntries):
            state.viewState = TrackerViewState(userFoodEntries: userEntries)
            state.entryToAdd.prepareForNextEntry()
            return .none
        case let .failure(error):
            state.errorMessage = error.message
            return .none
        }

    case let .setNavigationRoute(route):
        state.navigationRoute = route
        switch route {
        case .addFoodEntry:
            state.entryToAdd = AddFoodEntryState()
        case .setEntriesFilter:
            break
        case .none:
            break
        }

        return .none

    case .addFoodEntry(.addEntry):
        state.filterState.filterActive = false

        guard let dto = state.entryToAdd.postEntry else {
            return .none
        }

        return env.apiClient.apiRequest(
            route: Endpoint.addEntry(dto)
        ).receive(on: env.mainQueue)
        .catchToEffect(AddFoodEntryAction.addEntryResult)
        .map { TrackerAction.addFoodEntry($0) }

    case .addFoodEntry:
        return .none

    case let .foodEntriesFilter(.apply(range)):
        state.navigationRoute = nil
        state.viewState.clearDays()
        state.showProgress = true

        return env.apiClient.apiRequest(
            route: Endpoint.filterEntries(range),
            as: [FoodEntry].self
        )
        .receive(on: env.mainQueue)
        .catchToEffect(TrackerAction.foodEntriesFilterResult)
    case .foodEntriesFilter(.deactivate):
        return .init(value: .getFoodEntries)
    case .foodEntriesFilter:
        return .none
    case let .foodEntriesFilterResult(result):
        switch result {
        case let .success(entries):
            state.showProgress = false
            state.viewState.updateDays(entries)
            return .none
        case let .failure(error):
            state.errorMessage = error.message
            return .none
        }
    }
}
