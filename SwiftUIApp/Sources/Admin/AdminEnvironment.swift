import ComposableArchitecture
import Networking
import Models
import PhotoPicker

public struct AdminEnvironment {

    let apiClient: ApiClient
    let mainQueue: AnySchedulerOf<DispatchQueue>
    let dateNow: () -> Date
    let photoAssets: PhotoAssetsServiceType
    let firebase: FirebaseSDK

    public init(
        apiClient: ApiClient,
        mainQueue: AnySchedulerOf<DispatchQueue>,
        dateNow: @escaping () -> Date,
        photoAssets: PhotoAssetsServiceType,
        firebase: FirebaseSDK
    ) {
        self.apiClient = apiClient
        self.mainQueue = mainQueue
        self.dateNow = dateNow
        self.photoAssets = photoAssets
        self.firebase = firebase
    }
}

#if DEBUG

public extension AdminEnvironment {
    static var noop: AdminEnvironment {
        .init(
            apiClient: .noop,
            mainQueue: .immediate,
            dateNow: { Date() },
            photoAssets: PhotoAssetsServiceNoop(),
            firebase: FirebaseSDKNoop()
        )
    }
}

#endif
