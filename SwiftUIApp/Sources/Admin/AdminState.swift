import Models
import AddFoodEntry

public struct AdminState: Equatable {

    var entriesForUser = [UserFoodEntries]()
    var reportsViewState = ReportsState()
    var adminRoute: AdminRoute?
    var menu = Menu.allEntries
    var userToChange: UserFoodEntries?

    var addEntryViewState = AddFoodEntryState()

    var updateEntryViewState = AddFoodEntryState()
    var updateFoodEntry: FoodEntry?

    var showProgress: Bool = false
    var errorMessage: String?

    public init() {

    }
}

extension AdminState {
    var emptyScreenMessage: String {
        hasAnyFoodEntries ? "" : "No food entries, yet."
    }

    var hasAnyFoodEntries: Bool {
        entriesForUser.first { user in
            user.foodEntries.isEmpty == false
        } != nil
    }

    mutating func removeFoodEntryAt(section: Int, row: Int) -> FoodEntry {
        entriesForUser[section].removeFoodEntry(at: row)
    }

    mutating func clearNavigationState() {
        addEntryViewState = AddFoodEntryState()
        updateEntryViewState = AddFoodEntryState()
        updateFoodEntry = nil
        userToChange = nil
    }

    mutating func prepareForFoodEntryUpdate() {
        guard let entry = updateFoodEntry else {
            print("updateFoodEntry must not be nil")
            return
        }

        updateEntryViewState.subtitle = userToChange?.settings.email
        updateEntryViewState.name = entry.name
        updateEntryViewState.calories = entry.calories
        updateEntryViewState.photoUrl = entry.photoUrl
        updateEntryViewState.dateEaten = entry.date
    }
}
