import SwiftUI
import ComposableArchitecture
import DesignSystem
import Utility
import Models
import AddFoodEntry

public struct ReportsView: View {
    let viewStore: ViewStore<AdminState, AdminAction>

    public init(_ viewStore: ViewStore<AdminState, AdminAction>) {
        self.viewStore = viewStore
    }

    public var body: some View {
        VStack {
            VStack {
                Text("Entries added")
                .font(.title2)
                HStack(spacing: Space.large) {
                    entriesForWeek(
                        title: "One week ago",
                        number: viewStore.reportsViewState.lastSeven
                    )
                    entriesForWeek(
                        title: "Two weeks before",
                        number: viewStore.reportsViewState.weekBefore
                    )
                }

                if let error = viewStore.reportsViewState.weekReportError {
                    ErrorMessage(error)
                    .padding([.top, .bottom], Space.tiny)
                }
            }

            if viewStore.reportsViewState.caloriesAverage.count > 0 {
                Text("Calories per user for last 7 days")
                .font(.title2)
                .padding([.top], Space.small)
            }

            List(viewStore.reportsViewState.caloriesAverage) { item in
                HStack {
                    Text(item.email)
                    Spacer()
                    Text(Number.format(item.averageCalories))
                }
            }
            Spacer()
        }
        .overlay(alignment: .center) {
            NetworkActivity(
                show: viewStore.state.reportsViewState.isLoadingReports,
                scale: 1.5
            )
            .offset(y: -Space.xlarge)
        }
    }

    @ViewBuilder func entriesForWeek(title: String, number: String) -> some View {
        VStack {
            Text(title)
            .font(.title3)
            Text(number)
            .bold()
        }
    }

}
