import ComposableArchitecture

public let reportsReducer = Reducer<
    AdminState, AdminAction, AdminEnvironment
> { (state, action, env) in
    switch action {
    case .fetchReports:
        state.reportsViewState.weekReportError = nil
        state.reportsViewState.caloriesAverageError = nil

        state.reportsViewState.isLoadingWeekly = true
        state.reportsViewState.isLoadingAverage = true

        state.reportsViewState.caloriesAverage = []

        let weekReport = env.apiClient.apiRequest(
            route: Endpoint.weekReport,
            as: WeekReport.self
        ).receive(on: env.mainQueue)
        .catchToEffect(AdminAction.weekReportResult)

        let userAverage = env.apiClient.apiRequest(
            route: Endpoint.weeklyAverageCaloriesPerUser,
            as: [WeekCalorieAverage].self
        ).receive(on: env.mainQueue)
        .catchToEffect(AdminAction.userCalorieAverageResult)

        return Effect.merge(weekReport, userAverage)
    case let .weekReportResult(result):
        state.reportsViewState.isLoadingWeekly = false
        switch result {
        case let .success(report):
            state.reportsViewState.weekReport = report
        case let .failure(error):
            state.reportsViewState.weekReportError = error.message
        }
        return .none
    case let .userCalorieAverageResult(result):
        state.reportsViewState.isLoadingAverage = false
        switch result {
        case let .success(report):
            state.reportsViewState.caloriesAverage = report
            return .none
        case let .failure(error):
            state.reportsViewState.caloriesAverageError = error.message
            return .none
        }
    default:
        return .none
    }
}
