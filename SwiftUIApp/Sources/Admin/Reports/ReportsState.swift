struct ReportsState: Equatable {
    var isLoadingWeekly = false
    var isLoadingAverage = false

    var weekReport: WeekReport?
    var weekReportError: String?

    var caloriesAverage = [WeekCalorieAverage]()
    var caloriesAverageError: String?

    init() {

    }
}

extension ReportsState {
    var isLoadingReports: Bool {
        isLoadingWeekly || isLoadingAverage
    }

    var lastSeven: String {
        if let count = weekReport?.entriesWeekAgo {
            return "\(count)"
        } else {
            return "-"
        }
    }

    var weekBefore: String {
        if let count = weekReport?.entriesTwoWeeksAgo {
            return "\(count)"
        } else {
            return "-"
        }
    }
}
