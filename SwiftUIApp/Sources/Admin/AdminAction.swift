import Foundation
import Models
import AddFoodEntry

public enum AdminAction: Equatable {
    case fetchAllEntries
    case fetchAllEntriesResult(Result<[UserFoodEntries], ApiError>)

    case fetchReports
    case weekReportResult(Result<WeekReport, ApiError>)
    case userCalorieAverageResult(Result<[WeekCalorieAverage], ApiError>)

    case deleteFoodEntry(offsets: IndexSet, forUser: UserFoodEntries)
    case deleteFoodEntryResult(Result<FoodEntry, ApiError>)

    case addEntryForUser(AddFoodEntryAction)

    case updateEntryForUser(AddFoodEntryAction)

    case setUserToChange(UserFoodEntries)
    case setFoodEntryToChange(FoodEntry)
    case setNavigationRoute(AdminRoute?)
    case setMenu(Menu)
}
