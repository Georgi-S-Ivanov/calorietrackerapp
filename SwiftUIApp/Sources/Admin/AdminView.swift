import SwiftUI
import ComposableArchitecture
import DesignSystem
import Utility
import Models
import AddFoodEntry

public struct AdminView: View {
    let store: Store<AdminState, AdminAction>
    @ObservedObject var viewStore: ViewStore<AdminState, AdminAction>

    public init(_ store: Store<AdminState, AdminAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        VStack {
            Picker(
                "Menu",
                selection: .init(
                    get: { viewStore.menu },
                    set: { value in viewStore.send(.setMenu(value)) }
                )
            ) {
                Text("All Entries")
                .tag(Menu.allEntries)

                Text("Reports")
                .tag(Menu.reports)
            }
            .pickerStyle(.segmented)
            .padding([.top], Space.base)

            switch viewStore.menu {
            case .allEntries:
                allEntriesMenu()
            case .reports:
                ReportsView(viewStore)
            }
        }
    }

    @ViewBuilder func allEntriesMenu() -> some View {
        NavigationView {
            allEntriesContentOrEmpty()
            .navigationBarHidden(true)
            .background(
                navigationLinks()
            )
        }
        .overlay(alignment: .center) {
            NetworkActivity(
                show: viewStore.state.showProgress,
                scale: 1.5
            )
            .offset(y: -Space.xlarge)
        }
    }

    @ViewBuilder func allEntriesContentOrEmpty() -> some View {
        if viewStore.hasAnyFoodEntries {
            AnyView(content())
        } else {
            AnyView(Text(viewStore.emptyScreenMessage))
            .padding([.bottom], Space.xlarge * 3)
        }
    }

    @ViewBuilder func content() -> some View {
        List(viewStore.entriesForUser) { user in
            Section(header: sectionHeader(user)) {
                ForEach(user.foodEntries) { entry in
                    Button {
                        viewStore.send(.setFoodEntryToChange(entry))
                        viewStore.navigateToRoute(.updateEntryForUser, forUser: user)
                    } label: {
                        HStack {
                            Text("\(entry.name) (\(entry.calories))")
                            Spacer()
                            Text("\(DateFormat.short(entry.date))")
                        }
                    }
                    .accentColor(Color.black)
                }
                .onDelete(perform: {
                    viewStore.send(.deleteFoodEntry(offsets: $0, forUser: user))
                })
            }
        }
        .toolbar(content: {
            ToolbarItem(placement: .navigationBarTrailing) {
                EditButton()
            }
        })
    }

    @ViewBuilder func sectionHeader(_ user: UserFoodEntries) -> some View {
        HStack {
            Text("\(user.settings.email)")
            Spacer()
            Button {
                viewStore.navigateToRoute(.addEntryForUser, forUser: user)
            } label: {
                Image(systemName: "plus.circle")
            }
        }
    }

    @ViewBuilder func navigationLinks() -> some View {
        Group {
            NavigationLink(
                destination: AddFoodEntryView(
                    title: "Add entry",
                    store: store.scope(
                        state: \.addEntryViewState,
                        action: AdminAction.addEntryForUser
                    )
                ).navigationBarTitleDisplayMode(.inline),
                tag: AdminRoute.addEntryForUser,
                selection: viewStore.binding(
                    get: { $0.adminRoute },
                    send: AdminAction.setNavigationRoute
                )
            ) { }

            NavigationLink(
                destination: AddFoodEntryView(
                    title: "Update entry",
                    store: store.scope(
                        state: \.updateEntryViewState,
                        action: AdminAction.updateEntryForUser
                    )
                ).navigationBarTitleDisplayMode(.inline),
                tag: AdminRoute.updateEntryForUser,
                selection: viewStore.binding(
                    get: { $0.adminRoute },
                    send: AdminAction.setNavigationRoute
                )
            ) { }
        }
    }
}

extension ViewStore {
    func navigateToRoute(_ route: AdminRoute, forUser user: UserFoodEntries) where Action == AdminAction {
        send(AdminAction.setUserToChange(user))
        send(AdminAction.setNavigationRoute(route))
    }
}

#if DEBUG

struct AdminView_Previews: PreviewProvider {
    static var previews: some View {
        return AdminView(
            .init(
                initialState: .init(),
                reducer: adminReducer,
                environment: .noop
            )
        )

    }
}

#endif
