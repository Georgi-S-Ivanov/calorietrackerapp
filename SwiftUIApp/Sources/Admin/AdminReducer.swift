import ComposableArchitecture
import Models
import Networking
import AddFoodEntry

public let adminReducer = Reducer<
    AdminState, AdminAction, AdminEnvironment
>.combine(
    addFoodEntryReducer.pullback(
        state: \.addEntryViewState,
        action: /AdminAction.addEntryForUser,
        environment: {
            AddFoodEntryEnvironment(
                apiClient: $0.apiClient,
                mainQueue: $0.mainQueue,
                dateNow: $0.dateNow,
                photoAssets: $0.photoAssets,
                firebase: $0.firebase
            )
        }
    ),
    addFoodEntryReducer.pullback(
        state: \.updateEntryViewState,
        action: /AdminAction.updateEntryForUser,
        environment: {
            AddFoodEntryEnvironment(
                apiClient: $0.apiClient,
                mainQueue: $0.mainQueue,
                dateNow: $0.dateNow,
                photoAssets: $0.photoAssets,
                firebase: $0.firebase
            )
        }
    ),
    reportsReducer,
    mainAdminReducer
)

public let mainAdminReducer = Reducer<
    AdminState, AdminAction, AdminEnvironment
> { (state, action, env) in
    switch action {
    case .fetchAllEntries:
        if state.hasAnyFoodEntries == false {
            state.showProgress = true
        }

        return env.apiClient.apiRequest(
            route: Endpoint.fetchAllEntries,
            as: [UserFoodEntries].self
        ).receive(on: env.mainQueue)
        .catchToEffect(AdminAction.fetchAllEntriesResult)
    case let .fetchAllEntriesResult(result):
        state.showProgress = false
        switch result {
        case let .success(entriesForUser):
            state.entriesForUser = entriesForUser
            return .none
        case let .failure(error):
            state.errorMessage = error.message
            return .none

        }

    case let .deleteFoodEntry(offsets, user):
        guard let section = state.entriesForUser.firstIndex(of: user),
              let index = offsets.map({$0}).first
        else {
            state.errorMessage = "Could not delete entry."
            return .none
        }

        let user = state.entriesForUser[section]
        let toRemove = state.removeFoodEntryAt(section: section, row: index)

        return env.apiClient.apiRequest(
            route: Endpoint.deleteFoodEntry(
                userId: user.settings.userId,
                entryId: toRemove.id
            ), as: FoodEntry.self
        ).receive(on: env.mainQueue)
        .catchToEffect(AdminAction.deleteFoodEntryResult)

    case let .deleteFoodEntryResult(result):
        switch result {
        case let .success(entry):
            return .none
        case let .failure(error):
            state.errorMessage = error.message
            return .none
        }
    case let .setUserToChange(user):
        state.userToChange = user
        return .none

    case let .setNavigationRoute(route):
        state.adminRoute = route
        switch route {
        case .addEntryForUser:
            state.addEntryViewState.subtitle = state.userToChange?.settings.email
        case .updateEntryForUser:
            state.prepareForFoodEntryUpdate()
        case .none:
            state.clearNavigationState()
        }

        return .none

    case let .setMenu(menu):
        state.menu = menu

        switch menu {
        case .allEntries:
            return .none
        case .reports:
            return .init(value: .fetchReports)
        }

    case .addEntryForUser(.addEntry):
        guard let dto = state.addEntryViewState.postEntry else {
            return .none
        }

        guard let userId = state.userToChange?.settings.userId else {
            return .none
        }

        return env.apiClient.apiRequest(
            route: Endpoint.addEntryForUser(dto: dto, userId: userId)
        ).receive(on: env.mainQueue)
        .catchToEffect(AddFoodEntryAction.addEntryResult)
        .map { AdminAction.addEntryForUser($0) }

    case let .addEntryForUser(.addEntryResult(result)):
        switch result {
        case .success:
            state.addEntryViewState.prepareForNextEntry()
            return .init(value: .fetchAllEntries)
        case let .failure(error):
            // This error is handled in add food entry reducer
            return .none
        }

    case .addEntryForUser:
        return .none

    case .updateEntryForUser(.addEntry):

        guard let entry = state.updateFoodEntry else {
            return .none
        }

        guard let dto = state.updateEntryViewState.updateEntry(id: entry.id) else {
            return .none
        }

        guard let userId = state.userToChange?.settings.userId else {
            return .none
        }

        guard dto != UpdateFoodEntry(entry) else {
            state.updateEntryViewState.showProgress = false
            state.updateEntryViewState.successMessage = "Nothing changed."
            return .none
        }

        return env.apiClient.apiRequest(
            route: Endpoint.updateEntryForUser(dto: dto, userId: userId)
        ).receive(on: env.mainQueue)
        .catchToEffect(AddFoodEntryAction.addEntryResult)
        .map { AdminAction.updateEntryForUser($0) }

    case let .updateEntryForUser(.addEntryResult(result)):
        switch result {
        case .success:
            if let entry = state.updateFoodEntry {
                state.updateFoodEntry = state.updateEntryViewState.updateEntry(
                    id: entry.id
                )?.toApiModel
            }

            return .init(value: .fetchAllEntries)
        case let .failure(error):
            // This error is handled in add food entry reducer
            return .none
        }
    case .updateEntryForUser:
        return .none

    case let .setFoodEntryToChange(entry):
        state.updateFoodEntry = entry
        return .none
    case .fetchReports,
         .weekReportResult,
         .userCalorieAverageResult:
        // These actions are handled in reports reducer
        return .none
    }
}
