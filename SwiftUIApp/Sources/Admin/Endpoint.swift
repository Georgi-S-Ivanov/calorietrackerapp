import Foundation
import Networking
import Models
import AddFoodEntry
import Utility

enum Endpoint {
    case fetchAllEntries
    case deleteFoodEntry(userId: String, entryId: String)
    case addEntryForUser(dto: PostFoodEntry, userId: String )
    case updateEntryForUser(dto: UpdateFoodEntry, userId: String)
    case weekReport
    case weeklyAverageCaloriesPerUser
}

extension Endpoint: ApiRoute {
    var path: String {
        switch self {
        case .fetchAllEntries:
            return "admin/entries"
        case let .deleteFoodEntry(userId, entryId):
            return "admin/users/\(userId)/entries/\(entryId)"
        case let .addEntryForUser(_, userId):
            return "admin/users/\(userId)/entries"
        case let .updateEntryForUser(_, userId):
            return "admin/users/\(userId)/entries"
        case .weekReport:
            return "admin/entries/weekreport"
        case .weeklyAverageCaloriesPerUser:
            return "admin/entries/average"
        }
    }

    var method: HttpMethod {
        switch self {
        case .fetchAllEntries,
             .weekReport,
             .weeklyAverageCaloriesPerUser:
            return .get
        case .deleteFoodEntry:
            return .delete
        case .addEntryForUser:
            return .post
        case .updateEntryForUser:
            return .put
        }
    }

    var needsAuthorization: Bool {
        switch self {
        case .fetchAllEntries,
             .deleteFoodEntry,
             .addEntryForUser,
             .updateEntryForUser,
             .weekReport,
             .weeklyAverageCaloriesPerUser:
            return true
        }
    }

    var body: Data? {
        switch self {
        case .fetchAllEntries,
             .deleteFoodEntry,
             .weekReport,
             .weeklyAverageCaloriesPerUser:
            return nil
        case let .addEntryForUser(dto, _):
            return try? Json.encoder.encode(dto)
        case let .updateEntryForUser(dto, _):
            return try? Json.encoder.encode(dto)
        }
    }
}
