public enum Menu: String, CaseIterable, Identifiable {
    case allEntries
    case reports

    public var id: String { self.rawValue }
}
