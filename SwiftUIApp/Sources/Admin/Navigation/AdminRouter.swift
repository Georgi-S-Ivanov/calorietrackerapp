import Models

public enum AdminRoute: Equatable, Hashable {
    case addEntryForUser
    case updateEntryForUser
}
