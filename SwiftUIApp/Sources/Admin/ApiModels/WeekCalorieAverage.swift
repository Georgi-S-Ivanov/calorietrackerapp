public struct WeekCalorieAverage: Codable, Equatable, Identifiable {
    public let id: String
    let email: String
    let averageCalories: Double
}
