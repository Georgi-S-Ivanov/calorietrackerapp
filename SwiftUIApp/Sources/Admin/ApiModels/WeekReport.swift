public struct WeekReport: Codable, Equatable {
    let entriesWeekAgo: Int
    let entriesTwoWeeksAgo: Int
}
