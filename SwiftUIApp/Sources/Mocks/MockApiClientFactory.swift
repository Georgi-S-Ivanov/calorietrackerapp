import Foundation
import Networking
import Collections
import Combine

public class MockApiClientFactory {

    public typealias Response = (ApiRoute, ApiClient.Response)

    private var responses: Deque<Response>

    public init(_ responses: [Response]) {
        self.responses = Deque(responses)
    }

    public func createApiClient() -> ApiClient {
        ApiClient { [weak self] route in
            guard let self = self else {
                fatalError("Expect for self to be instantiated.")
            }
            
            assert(self.responses.count > 0, "No response for endpoint: \(route.path)")

            let response = self.responses.popFirst()!
            assert(
                route.path == response.0.path,
                "Expected to make request to \(response.0.path), instead got: \(route.path)"
            )
            return self.createResponse(response.1)

        } baseUrl: {
            URL(string: "http://mock.com")!
        }
    }
    
    deinit {
        assert(
            responses.count == 0,
            "Expected to make \(responses.count) more network requests.")
    }
}

extension MockApiClientFactory {
    func createResponse(_ resp: ApiClient.Response) -> AnyPublisher<ApiClient.Response, URLError> {
        return Future {
            $0(.success(resp))
        }.eraseToAnyPublisher()
    }

    static var failRequest: AnyPublisher<ApiClient.Response, URLError> {
        return Future {
            $0(
                .failure(
                    URLError.init(
                        URLError.Code.unknown
                    )
                )
            )
        }.eraseToAnyPublisher()
    }
}
