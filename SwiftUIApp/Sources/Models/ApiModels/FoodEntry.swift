import Foundation

public struct FoodEntry: Codable, Equatable, Identifiable {
    public let id: String
    public let date: Date
    public let name: String
    public let calories: Int
    public let photoUrl: String?

   public init(
        id: String,
        date: Date,
        name: String,
        calories: Int,
        photoUrl: String?
    ) {
        self.id = id
        self.date = date
        self.name = name
        self.calories = calories
        self.photoUrl = photoUrl
    }
}
