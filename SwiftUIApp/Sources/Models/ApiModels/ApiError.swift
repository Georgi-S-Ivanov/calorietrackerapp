import Foundation

public struct ApiError: Codable, Error, Equatable, LocalizedError {
    public let errorDump: String
    public let file: String
    public let line: UInt
    public let message: String
    public let statusCode: Int
    public let errors: [FirebaseApiError]?

    public init(
        error: Error,
        statusCode: Int = 0,
        file: StaticString = #fileID,
        line: UInt = #line
    ) {
        var string = ""
        dump(error, to: &string)
        self.errorDump = string
        self.statusCode = statusCode
        self.file = String(describing: file)
        self.line = line
        self.message = error.localizedDescription
        self.errors = nil
    }

    private init(
        message: String,
        statusCode: Int,
        firebaseErrors: [FirebaseApiError]? = nil,
        file: StaticString = #fileID,
        line: UInt = #line
    ) {
        self.errorDump = message
        self.statusCode = statusCode
        self.file = String(describing: file)
        self.line = line
        self.message = message
        self.errors = firebaseErrors
    }
    
    public func userErrorMessage() -> String {
        if let errors = errors {
            let output = errors.compactMap {
                FirebaseErrorMessages(rawValue: $0.message)?.toUserError()
            }.joined(separator: ", ")
            return output.isEmpty == false
            ? output
            : message
        } else {
            return message
        }
    }
}

extension ApiError {
    public static func notHttpResponse(
        file: StaticString = #fileID,
        line: UInt = #line) -> ApiError {
            ApiError(
                message: "Not an http response.",
                statusCode: 0,
                file: file,
                line: line
            )
    }

    public static func httpFail(
        statusCode: Int,
        file: StaticString = #fileID,
        line: UInt = #line) -> ApiError {
            ApiError(
                message: "Request failed with status \(statusCode).",
                statusCode: statusCode,
                file: file,
                line: line
            )
    }
    
    public static func parseHttpError(
        statusCode: Int,
        data: Data,
        file: StaticString = #fileID,
        line: UInt = #line
    ) -> ApiError {
        if let error = try? parseFirebaseError(
            statusCode: statusCode,
            data: data,
            file: file,
            line: line
        ) {
            return error
        } else {
            return httpFail(statusCode: statusCode, file: file, line: line)
        }
    }
    
    private static func parseFirebaseError(
        statusCode: Int,
        data: Data,
        file: StaticString,
        line: UInt
    ) throws -> ApiError {
        let firebaseErrorResponse = try JSONDecoder().decode(FirebaseApiErrorResponse.self, from: data)
        return .init(
            message: "Request failed with status \(statusCode).",
            statusCode: statusCode,
            firebaseErrors: firebaseErrorResponse.errors
        )
    }
}
