public struct UserFoodEntries: Codable, Equatable, Identifiable {
    public private(set) var foodEntries: [FoodEntry]
    public let settings: TrackerSettings

    public init() {
        foodEntries = []
        settings = TrackerSettings()
    }

    public var id: String {
        settings.userId
    }

    public mutating func removeFoodEntry(at index: Int) -> FoodEntry {
        foodEntries.remove(at: index)
    }
}
