public struct User: Codable, Equatable {
    public let id: String
    public let email: String
    public let token: String
    public let refreshToken: String
    public let expiresIn: String
    public let roles: [UserRole]
}

public extension User {
    var isAdmin: Bool {
        roles.contains { $0 == .admin }
    }

    var allRoles: String {
        roles.map {
            $0.rawValue.capitalized
        }.joined(separator: ", ")
    }
}
