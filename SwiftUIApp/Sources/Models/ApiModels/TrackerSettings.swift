import Foundation

public struct TrackerSettings: Codable, Equatable {
    public let email: String
    public let userId: String
    public let calorieLimit: Int

    public init() {
        self.email = ""
        self.userId = ""
        self.calorieLimit = 0
    }
}
