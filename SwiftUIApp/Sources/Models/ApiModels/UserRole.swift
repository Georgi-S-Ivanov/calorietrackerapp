public enum UserRole: String, Equatable, Codable {
    case user
    case admin
}
