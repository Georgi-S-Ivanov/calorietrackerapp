import Foundation

// Literals that the Firebase API returns for errors
// See: https://firebase.google.com/docs/reference/rest/auth
enum FirebaseErrorMessages: String {
    case emailExists = "EMAIL_EXISTS"
    case operationNotAllowed = "OPERATION_NOT_ALLOWED"
    case tooManyAttempts = "TOO_MANY_ATTEMPTS_TRY_LATER"
    case emailNotFound = "EMAIL_NOT_FOUND"
    case invalidPassword = "INVALID_PASSWORD"
    case userDisabled = "USER_DISABLED"
    
    func toUserError() -> String {
        switch self {
        case .emailExists:
            return "The email address is already in use by another account"
        case .operationNotAllowed:
            return "Password sign-in is disabled for this project"
        case .tooManyAttempts:
            return "We have blocked all requests from this device due to unusual activity. Try again later"
        case .emailNotFound:
            return "There is no user record corresponding to this identifier"
        case .invalidPassword:
            return "The password is invalid or the user does not have a password"
        case .userDisabled:
            return "The user account has been disabled by an administrator"
        }
    }
}

public struct FirebaseApiError: Codable, Error, Equatable {
    let message: String
    let domain: String
    let reason: String
}

public struct FirebaseApiErrorResponse: Codable, Error, Equatable {
    let errors: [FirebaseApiError]
    let statusCode: Int
}
