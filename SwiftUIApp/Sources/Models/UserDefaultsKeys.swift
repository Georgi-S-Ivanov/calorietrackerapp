import Foundation

public enum UserDefaultsKeys: String {
    case currentUser = "calorieTracker.signedUser"
}

public extension UserDefaults {
    func data(forKey: UserDefaultsKeys) -> Data? {
        data(forKey: forKey.rawValue)
    }

    func setValue(_ value: Codable, forKey: UserDefaultsKeys) {
        setValue(value, forKey: forKey.rawValue)
    }

    func removeObject(forKey: UserDefaultsKeys) {
        removeObject(forKey: forKey.rawValue)
    }
}
