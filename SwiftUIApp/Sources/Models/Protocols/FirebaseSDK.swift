import Foundation
import UIKit
import Combine

public protocol FirebaseSDK {
    func configure()
    func uploadImage(_ image: UIImage) -> AnyPublisher<URL, FirebaseSDKError>
}

#if DEBUG

public struct FirebaseSDKNoop: FirebaseSDK {
    public init() {
    }

    public func configure() {
    }

    public func uploadImage(_ image: UIImage) -> AnyPublisher<URL, FirebaseSDKError> {
        Future<URL, FirebaseSDKError>.init { promise in
            promise(.failure(.imageUploadError))
        }.eraseToAnyPublisher()
    }
}

#endif
