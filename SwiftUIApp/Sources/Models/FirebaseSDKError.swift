public enum FirebaseSDKError: Error {
    case imageUploadError

    public var message: String {
        switch self {
        case .imageUploadError:
            return "Could not upload image."
        }
    }
}
