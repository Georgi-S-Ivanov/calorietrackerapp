import SwiftUI
import ComposableArchitecture
import Models
import DesignSystem
import Utility

public struct ProfileView: View {

    let store: Store<ProfileState, ProfileAction>
    @ObservedObject var viewStore: ViewStore<ProfileState, ProfileAction>

    public init(store: Store<ProfileState, ProfileAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        NavigationView {
            VStack {
                ProfileRow(
                    title: "Profile",
                    systemImage: "person.circle",
                    value: viewStore.user.email
                )

                ProfileRow(
                    title: "Role",
                    systemImage: "person.icloud.fill",
                    value: viewStore.user.allRoles
                )

                if let settings = viewStore.settings {
                    ProfileRow(
                        title: "Calorie limit",
                        systemImage: "exclamationmark.octagon.fill",
                        value: Number.format(settings.calorieLimit)
                    )
                }

                Spacer()
            }
            .padding(Space.base)
            .navigationBarItems(
                trailing: Button {
                    viewStore.send(.logout)
                } label: {
                    Text("Logout")
                }
            )
        }

    }
}

struct ProfileRow: View {
    let title: String
    let systemImage: String
    let value: String

    var body: some View {
        VStack {
            HStack {
                Label(title, systemImage: systemImage)
                Spacer()
                Text(value)
            }
            Divider()
        }
        .padding(Space.small)
    }
}
