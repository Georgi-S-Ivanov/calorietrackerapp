import Models

public struct ProfileState: Equatable {
    public let user: User
    public let settings: TrackerSettings?

    public init(
        user: User,
        settings: TrackerSettings?
    ) {
        self.user = user
        self.settings = settings
    }
}
