import PhotosUI
import Combine

public class PhotoAssetsService: PhotoAssetsServiceType {
    public init() {

    }

    public func getAsset(_ pickResult: PhotoPickResult) -> AnyPublisher<UIImage, PhotoAssetsError> {
        let futurePublisher = Deferred {
            Future<UIImage, PhotoAssetsError> { [weak self] promise in
                self?.requestImage(pickResult.fetch, promise: promise)
            }
        }

        return futurePublisher.timeout(
            3,
            scheduler: DispatchQueue.main,
            customError: { .timeout }
        )
        .eraseToAnyPublisher()
    }

    private func requestImage(
        _ fetchResult: PHFetchResult<PHAsset>,
        promise: @escaping (Result<UIImage, PhotoAssetsError>) -> Void) {
        fetchResult.enumerateObjects { (asset, _, stop) -> Void in
            stop.pointee = true
            let options = PHImageRequestOptions()
            options.resizeMode = .fast
            let manager = PHImageManager.default()

            manager.requestImage(
                for: asset,
                targetSize: CGSize(width: 1000, height: 1000),
                contentMode: .aspectFill,
                options: options
            ) { (img, opt) in
                guard let isDegraded = opt?[PHImageResultIsDegradedKey] as? Bool,
                      isDegraded == false
                else {
                    return
                }

                if let img = img {
                    promise(.success(img))
                } else {
                    promise(.failure(.errorRetrievingImage))
                }
            }
        }
    }
}
