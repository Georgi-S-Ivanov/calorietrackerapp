import PhotosUI

public struct PhotoPickResult: Equatable {
    let fetchId: UUID
    let fetch: PHFetchResult<PHAsset>

    init(_ fetch: PHFetchResult<PHAsset>) {
        self.fetch = fetch
        self.fetchId = UUID()
    }
}
