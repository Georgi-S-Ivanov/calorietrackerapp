import SwiftUI
import PhotosUI

public typealias OnPickedPhoto = (PhotoPickResult) -> Void

public struct PhotoPicker: UIViewControllerRepresentable {

    let onPicked: OnPickedPhoto
    let onDismiss: () -> Void
    public init(
        onPicked: @escaping OnPickedPhoto,
        onDismiss: @escaping () -> Void
    ) {
        self.onPicked = onPicked
        self.onDismiss = onDismiss
    }

    public func makeUIViewController(
        context: UIViewControllerRepresentableContext<PhotoPicker>
    ) -> PHPickerViewController {
        let config = PHPickerConfiguration(photoLibrary: PHPhotoLibrary.shared())
        let controller = PHPickerViewController(configuration: config)
        controller.delegate = context.coordinator
        return controller
    }

    public func updateUIViewController(
        _ uiViewController: PHPickerViewController,
        context: UIViewControllerRepresentableContext<PhotoPicker>
    ) {

    }

    public static func dismantleUIViewController(_ uiViewController: PHPickerViewController, coordinator: Coordinator) {

    }

    public func makeCoordinator() -> Coordinator {
        Coordinator(
            onDismiss: onDismiss,
            onPicked: onPicked
        )
    }
}

public class Coordinator: PHPickerViewControllerDelegate {

    private let onDismiss: () -> Void
    private let onPicked: OnPickedPhoto

    init(onDismiss: @escaping () -> Void, onPicked: @escaping OnPickedPhoto) {
        self.onDismiss = onDismiss
        self.onPicked = onPicked
    }

    public func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        guard results.count > 0 else {
            onDismiss()
            return
        }

        let identifiers = results.compactMap(\.assetIdentifier)
        let fetchResult = PHAsset.fetchAssets(withLocalIdentifiers: identifiers, options: nil)
        onPicked(PhotoPickResult(fetchResult))
        onDismiss()
    }
}
