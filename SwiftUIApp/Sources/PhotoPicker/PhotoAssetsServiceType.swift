import UIKit
import Combine

/// Service protocol for retreiving photos from gallery
public protocol PhotoAssetsServiceType {

    /// Retrieves `single` image from ``PHFetchResult``
    /// Times out after 3 seconds if operation is delayed.
    /// - Returns: Publisher
    func getAsset(_ pickResult: PhotoPickResult) -> AnyPublisher<UIImage, PhotoAssetsError>
}

#if DEBUG

public class PhotoAssetsServiceNoop: PhotoAssetsServiceType {
    public init () {

    }

    public func getAsset(_ pickResult: PhotoPickResult) -> AnyPublisher<UIImage, PhotoAssetsError> {
        Future<UIImage, PhotoAssetsError>.init { promise in
            promise(.failure(PhotoAssetsError.errorRetrievingImage))
        }.eraseToAnyPublisher()
    }
}

#endif
