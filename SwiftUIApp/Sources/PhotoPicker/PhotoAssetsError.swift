public enum PhotoAssetsError: Error {
    case timeout
    case errorRetrievingImage

    public var message: String {
        switch self {
        case .timeout:
            return "Timeout during image retrieval."
        case .errorRetrievingImage:
            return "Could not retrieve image."
        }
    }
}
