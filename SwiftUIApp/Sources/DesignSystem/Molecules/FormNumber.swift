import SwiftUI

public struct FormNumber: View {
    let placeholder: String
    let numberBinding: Binding<Int>
    let errorMessage: String?

    public init(
        _ placeholder: String,
        numberBinding: Binding<Int>,
        errorMessage: String? = nil
    ) {
        self.placeholder = placeholder
        self.numberBinding = numberBinding
        self.errorMessage = errorMessage
    }

    public var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            createTextField()
            ErrorMessage(errorMessage)
        })
        .padding([.leading, .trailing], Space.xlarge)
        .padding([.top, .bottom], Space.small)
    }

    @ViewBuilder func createTextField() -> some View {
        TextField(
            placeholder,
            text: Binding<String>.init(
                get: { bindingValue(numberBinding.wrappedValue) },
                set: { (str) in
                    if let value = Int(str) {
                        numberBinding.wrappedValue = value
                    } else {
                        numberBinding.wrappedValue = 0
                        print("This field should contain only integers.")
                    }
                }
            )
        )
            .keyboardType(.numberPad)
    }

    func bindingValue(_ value: Int) -> String {
        if value == 0 {
            return ""
        } else {
            return "\(value)"
        }
    }
}
