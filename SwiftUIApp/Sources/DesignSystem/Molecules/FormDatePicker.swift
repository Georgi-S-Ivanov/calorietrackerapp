import SwiftUI

public struct FormDatePicker: View {

    let title: String
    let components: DatePickerComponents
    let dateBinding: Binding<Date>
    let errorMessage: String?

    public init(
        title: String,
        components: DatePickerComponents,
        dateBinding: Binding<Date>,
        errorMessage: String?
    ) {
        self.title = title
        self.components = components
        self.dateBinding = dateBinding
        self.errorMessage = errorMessage
    }

    public var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            createDatePicker()
            ErrorMessage(errorMessage)
        })
        .padding([.leading, .trailing], Space.large)
        .padding([.top, .bottom], Space.small)
    }

    @ViewBuilder func createDatePicker() -> DatePicker<Text> {
        DatePicker(
            title,
            selection: dateBinding,
            displayedComponents: components
        )
    }
}
