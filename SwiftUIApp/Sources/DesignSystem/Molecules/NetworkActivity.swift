import SwiftUI

public struct NetworkActivity: View {
    let scale: CGFloat
    let show: Bool

    public init(show: Bool, scale: CGFloat = 1) {
        self.show = show
        self.scale = scale
    }

    public var body: some View {
        if show {
            ProgressView()
                .progressViewStyle(.circular)
                .scaleEffect(scale)
        }
    }
}
