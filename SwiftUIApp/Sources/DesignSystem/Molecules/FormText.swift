import SwiftUI

public struct FormText: View {
    let placeholder: String
    let textBinding: Binding<String>
    let secure: Bool
    let errorMessage: String?

    public init(
        _ placeholder: String,
        textBinding: Binding<String>,
        errorMessage: String? = nil,
        secure: Bool = false
    ) {
        self.placeholder = placeholder
        self.textBinding = textBinding
        self.errorMessage = errorMessage
        self.secure = secure
    }

    public var body: some View {
        VStack(alignment: .leading, spacing: 0, content: {
            createTextField()
            ErrorMessage(errorMessage)
        })
        .padding([.leading, .trailing], Space.xlarge)
        .padding([.top, .bottom], Space.small)
    }

    @ViewBuilder func createTextField() -> some View {
        if secure {
            SecureField(placeholder, text: textBinding)
        } else {
            TextField(placeholder, text: textBinding)
        }
    }
}
