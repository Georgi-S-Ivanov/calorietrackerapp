import SwiftUI

public struct SuccessMessage: View {
    let message: String

    public init(_ message: String) {
        self.message = message
    }

    public var body: some View {
        Text(message)
        .foregroundColor(.green)
        .padding(Space.small)
    }
}
