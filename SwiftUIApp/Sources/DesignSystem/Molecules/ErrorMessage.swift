import SwiftUI
import Models

public struct ErrorMessage: View {
    let message: String?

    public init(_ message: String?) {
        self.message = message
    }

    public init(_ apiError: ApiError?) {
        self.message = apiError?.errorDescription
    }

    public var body: some View {
        if let message = message {
            Text(message)
                .font(.caption2)
                .foregroundColor(.red)
        }
    }
}
