import SwiftUI

public enum Space {

    /// Space equal to 1
    public static let border: CGFloat = 1

    /// Space equal to 8
    public static let tiny: CGFloat = 8

    /// Space equal to 16
    public static let small: CGFloat = 16

    /// Space equal to 24
    public static let base: CGFloat = 24

    /// Space equal to 40
    public static let large: CGFloat = 40

    /// Space equal to 60
    public static let xlarge: CGFloat = 60

}
