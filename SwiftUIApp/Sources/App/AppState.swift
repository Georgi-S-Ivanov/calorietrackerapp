import Models
import SignIn
import SignUp
import Tracker
import Admin
import Profile

public struct AppState: Equatable {
    var user: User?
    var signIn = SignInState()
    var signUp = SignUpState()
    var tracker = TrackerState()
    var admin = AdminState()

    var tabSelection = TabSelection.tracker

    public init() {

    }

    mutating func resetAllState() {
        user = nil
        signIn = SignInState()
        signUp = SignUpState()
        tracker = TrackerState()
        admin = AdminState()
        tabSelection = .tracker
    }

    var profile: ProfileState? {
        guard let user = user else {
            return nil
        }

        return ProfileState(user: user, settings: tracker.viewState.settings)
    }
}
