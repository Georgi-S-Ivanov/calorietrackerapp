import ComposableArchitecture
import Tracker
import Profile
import Admin

/// The purpose of this reducer is to update state accordingly in certain situations.
/// `For example:` when we receive a successful sign in response it assigns the user object
/// in the global app state, as the local reducer of sign in cannot do this.
///
let coreReducer = Reducer<AppState, AppAction, AppEnvironment> { state, action, env in
    switch action {
    case let .appLifecycle(action):
        switch action {
        case .didFinishLaunching:
            return .none
        case .loadDataOnFinishedLaunch:
            return .init(value: AppAction.tracker(.getFoodEntries))
        }
    case let .signIn(action):
        switch action {
        case .binding:
            return .none
        case .signIn:
            return .none
        case let .userSignedIn(result):
            guard let user = try? result.get() else {
                return .none
            }

            state.user = user
            _ = env.saveUser(user)

            return .init(value: AppAction.tracker(.getFoodEntries))
        case .setNavigation:
            return .none
        }
    case let .signUp(action):
        switch action {
        case .binding:
            return .none
        case .signUp:
            return .none
        case let .signUpRequest(result):
            if let user = try? result.get() {
                state.user = user
            }
            return .none
        }
    case let .tabSelection(selection):
        state.tabSelection = selection
        switch selection {
        case .tracker:
            return .none
        case .profile:
            return .none
        case .admin:
            return .init(value: AppAction.admin(.fetchAllEntries))
        }

    case let .profile(action):
        switch action {
        case .logout:
            state.resetAllState()
            _ = env.saveUser(nil)
            return .none
        }
    case let .tracker(action):
        switch action {
        case .getFoodEntries:
            return .none
        case .foodEntriesFilter:
            return .none
        case let .foodEntriesResult(result):
            switch result {
            case .success:
                return .none
            case let .failure(error):
                if error.statusCode == 401 {
                    state.resetAllState()
                }
                return .none
            }
        case .setNavigationRoute:
            return .none
        case let .addFoodEntry(.addEntryResult(result)):
            switch result {
            case .success:
                return .init(value: AppAction.tracker(.getFoodEntries))
            case .failure:
                return .none
            }
        case .addFoodEntry:
            return .none
        case .foodEntriesFilterResult:
            return .none
        }
    case .admin(.addEntryForUser(.addEntryResult)),
         .admin(.deleteFoodEntryResult),
         .admin(.updateEntryForUser(.addEntryResult)):
        return .init(value: AppAction.tracker(.getFoodEntries))
        
    case .admin:
        return .none
    }
}
