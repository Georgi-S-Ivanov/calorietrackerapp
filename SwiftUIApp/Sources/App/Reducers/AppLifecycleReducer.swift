import ComposableArchitecture
import Models

/// This reducer handles events related to the app cycle.
/// 
public let appLifecycleReducer = Reducer<AppState, AppLifecycleAction, AppEnvironment> { (state, action, env) in
    switch action {
    case .didFinishLaunching:
        env.firebase.configure()
        guard let user = env.loadUser() else {
            return .none
        }

        state.user = user
        return .init(value: .loadDataOnFinishedLaunch)
    case .loadDataOnFinishedLaunch:
        return .none
    }
}
