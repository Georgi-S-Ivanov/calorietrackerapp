import ComposableArchitecture
import SignIn
import SignUp
import Tracker
import Admin

/// This is the top-level app reducer, it's purpose is to combine all others.
/// The order in which they are placed matters for execution.
/// 
public let appReducer = Reducer<AppState, AppAction, AppEnvironment>.combine(
    signInReducer.pullback(state: \.signIn, action: /AppAction.signIn, environment: {
        SignInEnvironment(
            apiClient: $0.apiClient,
            mainQueue: $0.mainQueue
        )
    }),
    signUpReducer.pullback(state: \.signUp, action: /AppAction.signUp, environment: {
        SignUpEnvironment(
            apiClient: $0.apiClient,
            saveUser: $0.saveUser,
            mainQueue: $0.mainQueue
        )
    }),
    trackerReducer.pullback(state: \.tracker, action: /AppAction.tracker, environment: {
        TrackerEnvironment(
            apiClient: $0.apiClient,
            mainQueue: $0.mainQueue,
            dateNow: $0.dateNow,
            photoAssets: $0.photoAssets,
            firebase: $0.firebase
        )
    }),
    adminReducer.pullback(
        state: \.admin,
        action: /AppAction.admin,
        environment: {
            AdminEnvironment(
                apiClient: $0.apiClient,
                mainQueue: $0.mainQueue,
                dateNow: $0.dateNow,
                photoAssets: $0.photoAssets,
                firebase: $0.firebase
            )
        }
    ),
    appLifecycleReducer.pullback(state: \.self, action: /AppAction.appLifecycle, environment: { $0 }),
    coreReducer
)
