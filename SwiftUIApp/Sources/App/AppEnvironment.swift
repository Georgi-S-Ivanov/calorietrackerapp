import ComposableArchitecture
import Networking
import Models
import PhotoPicker

public struct AppEnvironment {

    let apiClient: ApiClient
    let saveUser: (User?) -> Bool
    let loadUser: () -> User?
    let mainQueue: AnySchedulerOf<DispatchQueue>
    let dateNow: () -> Date
    let firebase: FirebaseSDK
    let photoAssets: PhotoAssetsServiceType

    public init(
        apiClient: ApiClient,
        saveUser: @escaping (User?) -> Bool,
        loadUser: @escaping () -> User?,
        mainQueue: AnySchedulerOf<DispatchQueue>,
        dateNow: @escaping () -> Date,
        firebase: FirebaseSDK,
        photoAssets: PhotoAssetsServiceType
    ) {
        self.apiClient = apiClient
        self.saveUser = saveUser
        self.loadUser = loadUser
        self.mainQueue = mainQueue
        self.dateNow = dateNow
        self.firebase = firebase
        self.photoAssets = photoAssets
    }
}

#if DEBUG

public extension AppEnvironment {
    static var noop: AppEnvironment {
        .init(
            apiClient: .noop,
            saveUser: { _ in true },
            loadUser: { nil },
            mainQueue: .immediate,
            dateNow: { Date() },
            firebase: FirebaseSDKNoop(),
            photoAssets: PhotoAssetsServiceNoop()
        )
    }
}

#endif
