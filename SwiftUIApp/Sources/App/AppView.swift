import SwiftUI
import ComposableArchitecture
import DesignSystem
import SignIn
import SignUp
import Profile
import Tracker
import Admin

public struct AppView: View {
    let store: Store<AppState, AppAction>
    @ObservedObject var viewStore: ViewStore<AppState, AppAction>

    public init(_ store: Store<AppState, AppAction>) {
        self.store = store
        self.viewStore = ViewStore(store)
    }

    public var body: some View {
        guard let user = viewStore.user else {
            return AnyView(signUpOrSignInFlow())
        }

        return AnyView(
            TabView(
                selection: viewStore.binding(
                    get: { $0.tabSelection },
                    send: { .tabSelection($0) }
                )
            ) {
                TrackerView(store.scope(state: \.tracker, action: AppAction.tracker))
                .tabItem { Label("Tracker", systemImage: "c.circle.fill") }
                .tag(TabSelection.tracker)

                IfLetStore(store.scope(state: \.profile, action: AppAction.profile)) { store in
                    ProfileView(store: store)
                    .tabItem { Label("Profile", systemImage: "person.circle.fill") }
                    .tag(TabSelection.profile)
                }

                if user.isAdmin {
                    AdminView(store.scope(state: \.admin, action: AppAction.admin))
                    .tabItem { Label("Admin", systemImage: "a.circle.fill") }
                    .tag(TabSelection.admin)
                }
            }
        )
    }

    func signUpOrSignInFlow() -> some View {
        NavigationView {
            SignInView(
                store.scope(state: \.signIn, action: AppAction.signIn),
                signUpView: {
                    AnyView(SignUpView(store.scope(state: \.signUp, action: AppAction.signUp)))
                }
            )
            .padding([.bottom], Space.xlarge)
        }
    }
}

#if DEBUG

struct AppView_Previews: PreviewProvider {
    static var previews: some View {
        return AppView(
            .init(
                initialState: .init(),
                reducer: appReducer,
                environment: .noop
            )
        )

    }
}

#endif
