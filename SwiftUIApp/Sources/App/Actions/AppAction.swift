import SignIn
import SignUp
import Profile
import Tracker
import Admin

public enum AppAction: Equatable {
    case appLifecycle(AppLifecycleAction)
    case signIn(SignInAction)
    case signUp(SignUpAction)
    case profile(ProfileAction)
    case tracker(TrackerAction)
    case admin(AdminAction)
    case tabSelection(TabSelection)
}
