public enum AppLifecycleAction: Equatable {
    case didFinishLaunching
    case loadDataOnFinishedLaunch
}
