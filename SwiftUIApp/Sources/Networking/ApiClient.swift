import ComposableArchitecture
import Combine
import Models

public struct ApiClient {
    public typealias Response = (data: Data, response: URLResponse)

    let apiRequest: (ApiRoute) -> AnyPublisher<ApiClient.Response, URLError>
    let baseUrl: () -> URL

    public init(
        apiRequest: @escaping (ApiRoute) -> AnyPublisher<ApiClient.Response, URLError>,
        baseUrl: @escaping () -> URL
    ) {
        self.apiRequest = apiRequest
        self.baseUrl = baseUrl
    }

    public func apiRequest<A: Decodable>(
        route: ApiRoute,
        as: A.Type,
        file: StaticString = #file,
        line: UInt = #line
    ) -> AnyPublisher<A, ApiError> {
        createRequest(route)
        .tryMap { (data, response) in

            guard let httpResponse = response as? HTTPURLResponse else {
                throw ApiError.notHttpResponse()
            }

            let statusCode = httpResponse.statusCode
            guard (200..<300).contains(statusCode) else {
                throw ApiError.parseHttpError(statusCode: statusCode, data: data)
            }

            return (data, response)
        }
        .map { (data: Data, _: URLResponse) in data }
        .apiDecode(as: A.self, file: file, line: line)
        .eraseToAnyPublisher()
    }

    public func apiRequest(
        route: ApiRoute,
        file: StaticString = #file,
        line: UInt = #line
    ) -> AnyPublisher<HttpStatus, ApiError> {
        createRequest(route)
        .tryMap { (_, response) in

            guard let httpResponse = response as? HTTPURLResponse else {
                throw ApiError.notHttpResponse()
            }

            let status = HttpStatus(httpResponse.statusCode)
            guard status.isSuccessful else {
                throw ApiError.httpFail(statusCode: status.code)
            }

            return status
        }
        .mapError({ error in
            if let apiError = error as? ApiError {
                return apiError
            } else {
                return ApiError(error: error)
            }
        })
        .eraseToAnyPublisher()
    }

    private func createRequest(_ route: ApiRoute) -> AnyPublisher<ApiClient.Response, ApiError> {
        return self.apiRequest(route)
            .handleEvents(
                receiveOutput: {
#if DEBUG
                    print(
                """
                  API: route: \(route), \
                  status: \(($0.response as? HTTPURLResponse)?.statusCode ?? 0), \

                """
                //                receive data: \(String(decoding: $0.data, as: UTF8.self))
                    )
#endif
                }
            )
            .mapError({ urlError in
                ApiError(error: urlError)
            })
            .eraseToAnyPublisher()
    }
}

#if DEBUG
extension ApiClient {
    public static let noop = Self(
        apiRequest: { _ in Empty<ApiClient.Response, URLError>.noop() },
        baseUrl: { URL(string: "/")! }
    )
}

public extension Empty {
    static func noop<V, E>() -> AnyPublisher<V, E> {
        Empty<V, E>(completeImmediately: true).eraseToAnyPublisher()
    }
}
#endif
