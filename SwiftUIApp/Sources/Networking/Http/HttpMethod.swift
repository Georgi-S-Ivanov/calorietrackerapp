public enum HttpMethod: String {
    case `get`  = "GET"
    case patch  = "PATCH"
    case post   = "POST"
    case delete = "DELETE"
    case put    = "PUT"
}
