import Foundation

public protocol ApiRoute {
    var path: String { get }
    var method: HttpMethod { get }
    var needsAuthorization: Bool { get }
    var body: Data? { get }
}
