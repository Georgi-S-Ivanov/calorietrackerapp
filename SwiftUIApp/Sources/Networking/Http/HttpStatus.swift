public struct HttpStatus: Equatable {
    public let code: Int

    public init(_ code: Int) {
        self.code = code
    }

    public var isSuccessful: Bool {
        (200..<300) ~= code
    }
}
