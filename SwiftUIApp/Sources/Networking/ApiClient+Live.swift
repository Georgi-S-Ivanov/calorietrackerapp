import Models
import Combine
import ComposableArchitecture

extension ApiClient {

    public static func live(
        baseUrl: URL = URL(string: "https://europe-west1-calorietracker-fe8f5.cloudfunctions.net/api/")!
    ) -> Self {
        ApiClient(
            apiRequest: { route in
                beginRequest(baseUrl: baseUrl, route: route, authToken: {
                    let currentUser = UserDefaults.standard.data(forKey: UserDefaultsKeys.currentUser)
                        .flatMap({ try? Networking.jsonDecoder.decode(User.self, from: $0) })

                    return currentUser?.token
                })
            },
            baseUrl: { baseUrl }
        )
    }
}

private func makeURLRequest(for route: ApiRoute, from base: URL) -> URLRequest? {
    let components = URLComponents(string: route.path)
    let url = components?.url(relativeTo: base)
    var request = url.map { URLRequest(url: $0) }
    request?.httpMethod = route.method.rawValue
    request?.httpBody = route.body
    request?.addValue("application/json;charset=utf-8", forHTTPHeaderField: "Content-Type")

    return request
}

private func request(
    baseUrl: URL,
    route: ApiRoute,
    authToken: @escaping () -> String?
) -> Effect<(data: Data, response: URLResponse), URLError> {
    Deferred { () -> Effect<(data: Data, response: URLResponse), URLError> in
        guard var request = makeURLRequest(for: route, from: baseUrl) else {
            return .init(error: URLError(.badURL))
        }

        if route.needsAuthorization {
            guard let authToken = authToken() else {
                return .init(error: URLError(.userAuthenticationRequired))
            }

            request.addValue("Bearer \(authToken)", forHTTPHeaderField: "Authorization")
        }

        return URLSession.shared.dataTaskPublisher(for: request)
            .eraseToEffect()
    }
    .eraseToEffect()
}

private func beginRequest(
    baseUrl: URL,
    route: ApiRoute,
    authToken: @escaping () -> String?
) -> AnyPublisher<(data: Data, response: URLResponse), URLError> {
    return Deferred { () -> Effect<(data: Data, response: URLResponse), URLError> in
        return request(
            baseUrl: baseUrl,
            route: route,
            authToken: authToken
        )
    }
    .eraseToAnyPublisher()
}
