import Combine
import ComposableArchitecture
import Models

public enum Networking {
    public static let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
}

extension Publisher where Output == Data, Failure == Error {
  public func apiDecode<A: Decodable>(
    as type: A.Type,
    file: StaticString = #file,
    line: UInt = #line
  ) -> Effect<A, ApiError> {
    self
      .flatMap { data -> AnyPublisher<A, Error> in
        do {
            return try Just(Networking.jsonDecoder.decode(A.self, from: data))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        } catch let decodingError {
          do {
            return try Fail(
              error: Networking.jsonDecoder.decode(ApiError.self, from: data)
            ).eraseToAnyPublisher()
          } catch {
            return Fail(error: ApiError(error: decodingError)).eraseToAnyPublisher()
          }
        }
      }
      .mapError({ error in
          if let apiError = error as? ApiError {
              return apiError
          } else {
              return ApiError(error: error)
          }
      })
      .eraseToEffect()
  }
}
