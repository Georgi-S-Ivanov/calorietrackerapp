# Calorie Tracker App

It allows users to 
* Track their calorie intake by logging meals and pictures of them
* Filter for meals by date range
* See a warning when they exceed their calorie limit

Admins can
* See entries for all users 
* Edit or add new entries for a user
* See two reports - added entries for this vs the week before, calories per user for last 7 days


## Steps to run the iOS project

1. git clone https://Georgi-S-Ivanov@bitbucket.org/Georgi-S-Ivanov/calorietrackerapp.git

2. Open calorietrackerapp/SwiftUIApp/Project/CalorieTracker.xcodeproj

3. Wait for SPM to fetch all the dependencies and build the package graph

4. Pick a simulator device and run the project.

## Tests

To run all tests select CalorieTracker scheme and press Cmd + U

#### Potential issues

When fresh cloning the project sometimes SPM can't resolve all the 3rd party packages at once.

If you see errors like
= target 'gRPC-Core' referenced in product 'gRPC-Core' is empty
- Missing package product 'FirebaseStorageCombine-Community'
- Missing package product 'FirebaseStorage'

Open Package.swift and press Cmd + S - this will trigger rebuild of the dependency graph and will fix the issue.

