import XCTest
import ComposableArchitecture
import Networking
import Models
import Mocks
import Combine

@testable import SignUp

class SignUpReducerTests: XCTestCase {
    var env: SignUpEnvironment!
    
    override func setUp() {
        super.setUp()
        env = createEnvironment()
    }

    func test_emailValidation() {
        let store = TestStore(
            initialState: SignUpState(),
            reducer: signUpReducer,
            environment: env
        )

        store.send(.signUp) {
            $0.emailError = "Please enter valid email."
            $0.passwordError = "Please enter a password."
        }

        store.send(.set(\.$email, "unitTest")) {
            $0.email = "unitTest"
        }

        store.send(.signUp)

        store.send(.set(\.$email, "unitTest@test.com")) {
            $0.email = "unitTest@test.com"
        }

        store.send(.signUp) {
            $0.emailError = nil
        }
    }

    func test_passwordValidation() {
        let store = TestStore(
            initialState: SignUpState(),
            reducer: signUpReducer,
            environment: env
        )

        store.send(.set(\.$email, "unitTest@test.com")) {
            $0.email = "unitTest@test.com"
        }

        store.send(.set(\.$password, "12345")) {
            $0.password = "12345"
        }

        store.send(.signUp) {
            $0.passwordError = "Password must be at least 6 symbols."
        }

        store.send(.set(\.$password, "123456")) {
            $0.password = "123456"
        }

        store.send(.signUp) {
            $0.passwordError = "Passwords do not match."
        }

        store.send(.set(\.$confirmPassword, "123456")) {
            $0.confirmPassword = "123456"
        }

        store.send(.signUp) {
            $0.passwordError = nil
            $0.showProgress = true
        }
    }
    
    func test_signUp_success() {
        let dto = SignUpDto(
            email: "unitTest@test.com",
            password: "123456",
            confirmPassword: "123456",
            roles: [.admin]
        )
        
        let endpoint = Endpoint.signUp(dto: dto)
        
        let mockFactory = MockApiClientFactory(
            [
                MockApiClientFactory.Response(endpoint, (signUpResponse, okResponse))
            ]
        )
        
        env = createEnvironment(mockFactory.createApiClient())
        
        let store = TestStore(
            initialState: SignUpState(),
            reducer: signUpReducer,
            environment: env
        )
        
        store.send(.set(\.$email, "unitTest@test.com")) {
            $0.email = "unitTest@test.com"
        }
        store.send(.set(\.$password, "123456")) {
            $0.password = "123456"
        }
        store.send(.set(\.$confirmPassword, "123456")){
            $0.confirmPassword = "123456"
        }
        store.send(.set(\.$isAdmin, true)){
            $0.isAdmin = true
        }
        
        store.send(.signUp) { state in
            state.showProgress = true
        }
        
        store.receive(.signUpRequest(.success(signUpUser))) { state in
            state.showProgress = false
        }
    }
    
    func test_signUp_failure() {
        let dto = SignUpDto(
            email: "unitTest@test.com",
            password: "123456",
            confirmPassword: "123456",
            roles: [.admin]
        )
        
        let endpoint = Endpoint.signUp(dto: dto)
        
        let mockFactory = MockApiClientFactory(
            [
                MockApiClientFactory.Response(endpoint, (signUpResponse, serverError))
            ]
        )
        
        env = createEnvironment(mockFactory.createApiClient())
        
        let store = TestStore(
            initialState: SignUpState(),
            reducer: signUpReducer,
            environment: env
        )
        
        store.send(.set(\.$email, "unitTest@test.com")) {
            $0.email = "unitTest@test.com"
        }
        store.send(.set(\.$password, "123456")) {
            $0.password = "123456"
        }
        store.send(.set(\.$confirmPassword, "123456")){
            $0.confirmPassword = "123456"
        }
        store.send(.set(\.$isAdmin, false))
        store.send(.signUp) { state in
            state.showProgress = true
        }
        store.receive(.signUpRequest(.failure(apiError))) { state in
            state.signUpRequestError = "Request failed with status 500."
            state.showProgress = false
        }
    }
}

extension SignUpReducerTests {
    
    func createEnvironment(
        _ apiClient: ApiClient = .noop
    ) -> SignUpEnvironment {
        SignUpEnvironment(
            apiClient: apiClient,
            saveUser: {_ in true },
            mainQueue: .immediate
        )
    }
    
    var signUpResponse: Data {
        let payload = """
        {
            "id": "QCsqXi1uFdOItMxLSvFNVYRYEkq1",
            "email": "unitTest@test.com",
            "token": "jwt.token.data",
            "refreshToken": "refresh.token.data",
            "expiresIn": "3600",
            "roles": [
                "admin"
            ]
        }
        """
        return Data(payload.utf8)
    }
    
    var signUpUser: User {
        try! JSONDecoder().decode(User.self, from: signUpResponse)
    }
    
    var okResponse: URLResponse {
        HTTPURLResponse(
            url: URL(string: "http://mock.com")!,
            statusCode: 200,
            httpVersion: nil,
            headerFields: nil
        )!
    }
    
    var serverError: URLResponse {
        HTTPURLResponse(
            url: URL(string: "http://mock.com")!,
            statusCode: 500,
            httpVersion: nil,
            headerFields: nil
        )!
    }
    
    var apiError: ApiError {
        let response = """
        {
            "errorDump": "Request failed with status 500.",
            "file":  "Networking/ApiClient.swift",
            "line": 34,
            "message": "Request failed with status 500.",
            "statusCode": 500,
            "errors": null
        }
"""
        return try! JSONDecoder().decode(
            ApiError.self,
            from: Data(response.utf8)
        )
    }
}
