//
//  CalorieTrackerApp.swift
//  CalorieTracker
//
//  Created by Georgi-Ivanov on 9.12.21.
//

import SwiftUI
import ComposableArchitecture
import App

@main
struct CalorieTrackerApp: App {

    @UIApplicationDelegateAdaptor(AppDelegate.self) private var appDelegate

    var body: some Scene {
        WindowGroup {
            AppView(appDelegate.store)
        }
    }
}
