import Foundation
import FirebaseStorage
import Firebase
import Models
import UIKit
import Combine

class FirebaseService: FirebaseSDK {
    func configure() {
        FirebaseApp.configure()
    }

    func uploadImage(_ image: UIImage) -> AnyPublisher<URL, FirebaseSDKError> {
        let future = Deferred {
            Future<URL, FirebaseSDKError> { [weak self] promise in
                self?.firebaseUpload(image, promise: promise)
            }
        }

        return future.eraseToAnyPublisher()
    }

    private func firebaseUpload(
        _ image: UIImage,
        promise: @escaping (Result<URL, FirebaseSDKError>) -> Void
    ) {
        let storageRef = Storage.storage().reference()
        let foodEntry = storageRef.child(UUID().uuidString)

        let data = image.pngData()!

        foodEntry.putData(data) { info, _ in
            guard info != nil else {
                promise(.failure(.imageUploadError))
                return
            }

            foodEntry.downloadURL { url, _ in
                guard let url = url else {
                    promise(.failure(.imageUploadError))
                    return
                }
                promise(.success(url))
            }
        }
    }
}
