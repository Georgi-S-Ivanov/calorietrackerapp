//
//  AppEnvironment+Extensions.swift
//  CalorieTracker
//
//  Created by Georgi-Ivanov on 10.12.21.
//

import Foundation
import App
import Utility
import PhotoPicker

extension AppEnvironment {
    public static var live: Self {
        Self(
            apiClient: .live(),
            saveUser: { Utility.Persistance.save($0, withKey: .currentUser) },
            loadUser: { Utility.Persistance.load(forKey: .currentUser) },
            mainQueue: .main,
            dateNow: { Date() },
            firebase: FirebaseService(),
            photoAssets: PhotoAssetsService()
        )
    }
}
